package com.hedgehog.games.world;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//This class helps with some method and functions in WorldBuilder algorithm, line of sight algorithm and field of view
public class Point {

    public int x;
    public int y;
    public int z;

    public Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //Two points that represent the same location should be treated as equal
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof Point)) return false;

        Point point = (Point) o;

        if (x != point.x) return false;
        if (y != point.y) return false;
        return z == point.z;
    }

    //Two points that represent the same location should be treated as equal.
    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + x;
        result = 31 * result + y;
        result = 31 * result + z;
        return result;
    }

    public List<Point> neighborsOfPoint() {
        List<Point> points = new ArrayList<Point>();

        for(int xx = -1; xx < 2; xx++){
            for(int yy = -1; yy < 2; yy++){
                if(xx == 0 && yy == 0)
                    continue;

                points.add(new Point(x + xx, y + yy, z));
            }
        }

        Collections.shuffle(points);
        return points;
    }
}
