package com.hedgehog.games.creatures;

import com.hedgehog.games.rpgmechanics.LineOfSight;
import com.hedgehog.games.world.Point;
import com.hedgehog.games.world.Tile;

//Parent class for future player and creature AI
public class CreatureAI {

    public Creature creature;

    public CreatureAI(Creature creature) {
        this.creature = creature;
        this.creature.setCreatureAI(this);
    }

    public void onEnter(int creatureX, int creatureY, int creatureZ, Tile tile) {
        if (tile.isFloor()) {
            creature.x = creatureX;
            creature.y = creatureY;
            creature.z = creatureZ;
        } else {
            creature.doAction("bumps into a wall");
        }

    }

    protected void wander(int min, int max, int wanderTimes) {
        if (wanderTimes > 0) {
            boolean isHeroNearby;
            isHeroNearby = checkSurroundings();
            if (!isHeroNearby) {
                moveToRandomDirection(min, max);
            }
            --wanderTimes;
            wander(min, max, wanderTimes);
        }
    }

    protected boolean checkSurroundings() {
        Creature other;
        for (int xx = -1; xx < 2; xx++) {
            for (int yy = -1; yy < 2; yy++) {
                if (xx == 0 && yy == 0)
                    continue;

                other = creature.creature(this.creature.x + xx, this.creature.y + yy, this.creature.z);

                if (other != null && other.getSymbol() == '@') {
                    attackHero(other);
                    return true;
                }
            }
        }
        return false;
    }

    private void attackHero(Creature other) {
        creature.attack(other);
    }

    void moveToRandomDirection(int min, int max) {
        Creature other;
        int xCoordinateChange;
        int yCoordinateChange;

        do {
            xCoordinateChange = min + (int) (Math.random() * ((max - min) + max));
            yCoordinateChange = min + (int) (Math.random() * ((max - min) + max));
        } while (xCoordinateChange == 0 && yCoordinateChange == 0);

        int newCoordinateZ = this.creature.z;
        int newCoordinateX = creature.x + xCoordinateChange;
        int newCoordinateY = creature.y + yCoordinateChange;

        other = creature.creature(creature.x + xCoordinateChange,
                creature.y + yCoordinateChange, creature.z);

        if (other != null && other.getSymbol() != '@') {
            return;
        }

        if (creature.canEnter(newCoordinateX, newCoordinateY, newCoordinateZ)) {
            creature.moveBy(xCoordinateChange, yCoordinateChange, 0);
        }
    }


    //this method has an empty body, because each creature has a different way of implementing this method
    public void onUpdate() {
    }

    //this method has an empty body, because each creature has a different way of implementing this method
    public void onNotify(String format) {
    }

    public boolean canSee(int worldX, int worldY, int worldZ) {
        if (creature.z != worldZ)
            return false;

        if ((creature.x - worldX) * (creature.x - worldX) +
                (creature.y - worldY) * (creature.y - worldY) >
                creature.getLineOfSight() * creature.getLineOfSight())
            return false;

        for (Point p : new LineOfSight(creature.x, creature.y, worldX, worldY)) {
            if (creature.tile(p.x, p.y, worldZ).isFloor() ||
                    p.x == worldX && p.y == worldY)
                return false;
        }
        return true;
    }

    protected int randomizeCoordinate(int min, int max) {
        return (min + (int) (Math.random() * ((max - min) + 1)));
    }
}

