package com.hedgehog.games.creatures.creatureHelpers;

import com.hedgehog.games.creatures.creatureHelpers.raceattributes.*;

public class Attributes {

    private int strength;
    private int agility;
    private int stamina;
    private int intellect;
    private int spirit;


    public Attributes(String race) {
        CreatureAttributes creatureAttributes = CreatureStatisticsFactory.create(race);
        this.agility = creatureAttributes.getAgility();
        this.intellect = creatureAttributes.getIntellect();
        this.spirit = creatureAttributes.getSpirit();
        this.stamina = creatureAttributes.getStamina();
        this.strength = creatureAttributes.getStrength();

    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getIntellect() {
        return intellect;
    }

    public void setIntellect(int intellect) {
        this.intellect = intellect;
    }

    public int getSpirit() {
        return spirit;
    }

    public void setSpirit(int spirit) {
        this.spirit = spirit;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int changeStrength(int amount){
        return this.strength += amount;
    }

    public int changeAgility(int amount){
        return this.agility += amount;
    }

    public int changeStamina(int amount){
        return this.stamina += amount;
    }

    public int changeIntellect(int amount){
        return this.intellect += amount;
    }

    public int changeSpirit(int amount){
        return this.spirit += amount;
    }

}
