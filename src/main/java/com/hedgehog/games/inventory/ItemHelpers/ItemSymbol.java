package com.hedgehog.games.inventory.ItemHelpers;

public class ItemSymbol {

    /**
     * OTHER ITEMS
     */
    private final char O_ROCK = ',';
    private final char S_NULL = (char)189;
    private final char S_ANCIENT = (char)155;

    /**
     * ARMORS
     */
    private final char A_CLOTH = (char)134;
    private final char A_LEATHER = (char)133;
    private final char A_MAIL = (char)132;
    private final char A_PLATE = (char)131;

    /**
     * WEAPONS
     */
    private final char W_AXE = '7';
    private final char W_DAGGER = (char)161;
    private final char W_MACE = (char)139;
    private final char W_SWORD = 'l';
    private final char W_STAFF = '|';
    private final char W_POLEARM = (char)140;

    public char A_CLOTH() {
        return A_CLOTH;
    }

    public char A_LEATHER() {
        return A_LEATHER;
    }

    public char A_MAIL() {
        return A_MAIL;
    }

    public char A_PLATE() {
        return A_PLATE;
    }

    public char W_AXE() {
        return W_AXE;
    }

    public char W_DAGGER() {
        return W_DAGGER;
    }

    public char W_MACE() {
        return W_MACE;
    }

    public char W_SWORD() {
        return W_SWORD;
    }

    public char W_STAFF() {
        return W_STAFF;
    }

    public char W_POLEARM() {
        return W_POLEARM;
    }

    public char S_NULL() {
        return S_NULL;
    }

    public char S_ANCIENT() {
        return S_ANCIENT;
    }
}
