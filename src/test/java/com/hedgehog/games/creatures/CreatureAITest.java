package com.hedgehog.games.creatures;
import org.junit.Assert;
import org.junit.Test;
import com.hedgehog.games.world.World;
import com.hedgehog.games.world.WorldBuilderCustomLevel;

public class CreatureAITest {

    @Test
    public void wander_noOthers() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .build();
        CreatureAI ai = new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;

        ai.wander(-1, 1, 1);

        boolean result = (creature.x != oldX || creature.y != oldY) ? true : false;

        Assert.assertEquals(expectedResult, result);

        world.remove(creature);


    }

    @Test
    public void wander_otherMonsters() throws Exception {
        World world = new WorldBuilderCustomLevel(2,2,1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .withAttDice(1).withNoOfAttDice(1).withAttBonus(1)
                .build();
        Creature other = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .build();
        CreatureAI ai = new CreatureAI(creature);

        world.addAt(creature, 1, 1, 0);
        world.addAt(other, 0,0,0);

        boolean expectedResult = true;

        ai.wander(-1, 1, 1);

        boolean result = (other.getHitPoints() < 1) ? false : true;

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void wander_otherHero() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .build();
        Creature other = new Creature.CreatureBuilder(world, "Hero", '@', 1 , 1)
                .build();
        CreatureAI ai = new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);
        world.addAtEmptyLocation(other,0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;

        ai.wander(-1, 1, 1);

        boolean result = (creature.x == oldX && creature.y == oldY) ? true : false;

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void wander_otherHero_attacked_withNoDmG() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .build();
        Creature other = new Creature.CreatureBuilder(world, "Hero", '@', 1 , 1)
                .build();
        CreatureAI ai = new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);
        world.addAtEmptyLocation(other,0);

        boolean expectedResult = true;
        int heroStartHP = other.getHitPoints();

        ai.wander(-1, 1, 1);

        boolean result = (other.getHitPoints() == heroStartHP) ? true : false;

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void wander_otherHero_attacked_withDmg() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .withNoOfAttDice(1).withAttDice(1)
                .build();
        Creature other = new Creature.CreatureBuilder(world, "Hero", '@', 1 , 1)
                .build();
        CreatureAI ai = new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);
        world.addAtEmptyLocation(other,0);

        boolean expectedResult = true;
        int heroStartHP = other.getHitPoints();

        ai.wander(-1, 1, 1);

        boolean result = (other.getHitPoints() < heroStartHP) ? true : false;

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void checkSurroundings_noOthers() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .build();
        CreatureAI ai = new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        boolean expectedResult = false;

        boolean result = ai.checkSurroundings();

        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void checkSurroundings_otherMonster() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .build();
        Creature other = new Creature.CreatureBuilder(world, "other", 'o', 1, 1)
                .build();
        CreatureAI ai = new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);
        world.addAtEmptyLocation(other,0);

        boolean expectedResult = false;

        boolean result = ai.checkSurroundings();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void checkSurroundings_otherHero() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .build();
        Creature other = new Creature.CreatureBuilder(world, "Hero", '@', 1, 1)
                .build();
        CreatureAI ai = new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);
        world.addAtEmptyLocation(other,0);

        boolean expectedResult = true;

        boolean result = ai.checkSurroundings();

        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void moveToRandomDirection() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .build();
        CreatureAI ai = new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;



        ai.moveToRandomDirection(-1, 1);

        boolean result = (creature.x != oldX || creature.y != oldY) ? true : false;

        if(!result) {
            System.out.println(oldX + " " + oldY);
            System.out.println(creature.x + " " + creature.y);
        }

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creatureAllTest_10000times() throws Exception {
        for (int i = 0; i < 10000; i++) {
            wander_noOthers();
            wander_otherHero();
            wander_otherHero_attacked_withDmg();
            wander_otherHero_attacked_withNoDmG();
            wander_otherMonsters();
            moveToRandomDirection();
            checkSurroundings_noOthers();
            checkSurroundings_otherHero();
            checkSurroundings_otherMonster();
        }
    }

}