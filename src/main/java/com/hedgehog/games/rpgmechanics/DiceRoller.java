package com.hedgehog.games.rpgmechanics;

import java.util.Random;

//Simple Diceroller adopted from pen and paper RPG games like Dungeon and Dragons.
public class DiceRoller {

    private Random roller;

    public DiceRoller() {
        this.roller = new Random();
    }

    public int diceRoll(int numberOfRolls, int diceSize){
        return roll(numberOfRolls, diceSize, 0);
    }

    public int diceRoll(int numberOfRolls, int diceSize, int bonusPerRoll){
        return roll(numberOfRolls, diceSize, bonusPerRoll);
    }

    private int roll(int numberOfRolls, int diceSize, int bonusPerRoll) {
        if(numberOfRolls == 0 || diceSize == 0){
            return 0;
        } else {
            return rollResult(numberOfRolls, diceSize, bonusPerRoll);
    }
    }

    private int rollResult(int numberOfRolls, int diceSize, int bonusPerRoll) {
        int result = 0;
        for (int i = 1; i <= numberOfRolls; i++) {
            result += roller.nextInt(diceSize) + 1;
            result += bonusPerRoll;
        }
        if(result < 0){
            return 0;
        }
        return result;
    }
}

