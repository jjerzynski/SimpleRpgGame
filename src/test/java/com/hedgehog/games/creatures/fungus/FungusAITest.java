package com.hedgehog.games.creatures.fungus;

import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.creatures.CreatureMaker;
import org.junit.Assert;
import org.junit.Test;
import com.hedgehog.games.world.World;
import com.hedgehog.games.world.WorldBuilderCustomLevel;

public class FungusAITest {

    @Test
    public void spreadTest_hardCodedSpread(){

        World world = new WorldBuilderCustomLevel(7,7,1).makeLvlWithPattern("floor").build();
        CreatureMaker cm = new CreatureMaker(world);
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1 , 1)
                .build();
        FungusAI ai = new FungusAI(creature, cm);
        world.addAt(creature, 3, 3, 0);

        int expectedResult = 2;

        ai.spreadTest();
        int result = world.getCreaturesList().size();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void spreadTest_1000times(){
        for (int i = 0; i < 1000; i++) {
            spreadTest_hardCodedSpread();

        }
    }
}