package com.hedgehog.games.world;


import org.junit.Assert;
import org.junit.Test;

public class WorldBuilderTest {

    @Test
    public void GenericWorld_notNull() {
        int w = 3;
        int h = 3;
        int tilesNotNull = 0;
        WorldBuilder wb = new WorldBuilder(w, h, 1).makeCaves(0);

        int expectedResult = 9;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (wb.getTile(i, j, 0) != null)
                    tilesNotNull++;
            }
        }

        int result = tilesNotNull;

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void GenericWorld_caveToSmall() {
        int w = 5;
        int h = 5;
        int floorTiles = 0;
        WorldBuilder wb = new WorldBuilder(w, h, 1).makeCaves(0);

        int expectedResult = 0;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (wb.getTile(i, j, 0) == Tile.FLOOR)
                    floorTiles++;
            }
        }

        int result = floorTiles;
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void GenericWorld_optimalCaveSize() {
        int w = 20;
        int h = 20;
        int floors = 0;
        WorldBuilder wb = new WorldBuilder(w, h, 1).makeCaves(0);

        boolean expectedResult = true;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (wb.getTile(i, j, 0) == Tile.FLOOR)
                    floors++;
            }
        }

        boolean result = floors >= 25;
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void GenericWorlds_notOnlyFloors(){
        int w = 20;
        int h = 20;
        int floors = 0;
        WorldBuilder wb = new WorldBuilder(w, h, 1).makeCaves(3);

        boolean expectedResult = true;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (wb.getTile(i, j, 0) != Tile.FLOOR)
                    floors++;
            }
        }

        boolean result = floors < w*h;
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void GenericWorlds_notOnlyWalls(){
        int w = 20;
        int h = 20;
        int walls = 0;
        WorldBuilder wb = new WorldBuilder(w, h, 1).makeCaves(3);

        boolean expectedResult = true;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (wb.getTile(i, j, 0) != Tile.WALL)
                    walls++;
            }
        }

        boolean result = walls < w*h;
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void genericDungeonTest_1000times(){
        for (int i = 0; i < 1000; i++) {
            GenericWorld_notNull();
            GenericWorld_caveToSmall();
            GenericWorld_optimalCaveSize();
            GenericWorlds_notOnlyFloors();
            GenericWorlds_notOnlyWalls();

        }
    }

    @Test
    public void customWorld_genericLvlTest(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1).makeLvlWithPattern("oryginal");
        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.FLOOR ||
                        wb.tiles[i][j][0] == Tile.WALL)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);

    }

    @Test
    public void customWorld_validate_genericLvlTest_1000times(){
        for (int i = 0; i < 1000; i++) {
            customWorld_genericLvlTest();
        }
    }

    @Test
    public void customWorld_isFloor(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor");
        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.FLOOR)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void customWorld_IsWall(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1).makeLvlWithPattern("wall");
        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.WALL)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void customWorld_onlyOneFloor(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onefloor");
        int expectedResult = 1;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.FLOOR)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void customWorld_onlyOneWall(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall");
        int expectedResult = 1;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.WALL)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void customWorld_smoothLevelTest_floors_onewall(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall")
                .smoothWorldTest(1);

        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.FLOOR)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);

    }

    @Test
    public void customWorld_smoothLevelTest_walls_onefloor(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onefloormiddle")
              .smoothWorldTest(1);

        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.WALL)
                    result++;
            }
        }
        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void customWorld_smoothLevel_1wallLineMiddle_2floorlines(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1).makeLvlWithPattern("walllinemiddle")
                .smoothWorldTest(2);

        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.WALL)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);

    }

    @Test
    public void customWorld_smoothLevel_1floorLineMiddle_2wallLines(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floorlinetop")
                .smoothWorldTest(2);

        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.WALL)
                    result++;
            }
        }
        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void customWorld_smoothLevel_1floorLineNotMiddle_2wallLines(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1)
                .makeLvlWithPattern("floorlineright")
                .smoothWorldTest(2);

        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.WALL)
                    result++;
            }
        }
        Assert.assertEquals(expectedResult,result);

    }

    @Test
    public void customWorld_smoothLevel_1wallLineNotMiddle_2floorLines(){
        WorldBuilderCustomLevel wb = new WorldBuilderCustomLevel(1)
                .makeLvlWithPattern("walllineright")
                .smoothWorldTest(2);

        int expectedResult = 3;
        int result = 0;

        for (int i = 0; i < wb.getWidth(); i++) {
            for (int j = 0; j < wb.getHeight(); j++) {
                if(wb.tiles[i][j][0] == Tile.WALL)
                    result++;
            }
        }
        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void customLvlAllTests_1000times(){
        for (int i = 0; i < 1000; i++) {
            customWorld_isFloor();
            customWorld_IsWall();
            customWorld_onlyOneFloor();
            customWorld_onlyOneWall();
            customWorld_smoothLevel_1floorLineMiddle_2wallLines();
            customWorld_smoothLevel_1floorLineNotMiddle_2wallLines();
            customWorld_smoothLevel_1wallLineNotMiddle_2floorLines();
            customWorld_smoothLevel_1wallLineMiddle_2floorlines();

        }
    }
}


