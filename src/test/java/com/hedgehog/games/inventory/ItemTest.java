package com.hedgehog.games.inventory;

import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;

public class ItemTest {
    @Test
    public void modifyAttackValue() throws Exception {
        Item item = new Item('i', Color.RED, "item", 1, 1);
        item.modifyAttackValue(1);

        Assert.assertEquals(2, item.getAttackValue());
    }

    @Test
    public void modifyDefenseValue() throws Exception {
        Item item = new Item('i', Color.RED, "item", 1, 1);
        item.modifyDefenseValue(1);

        Assert.assertEquals(2, item.getDefenseValue());
    }

}