package com.hedgehog.games.creatures.creatureHelpers.raceattributes;

public class FungusAttributes extends CreatureAttributes {
    private int strength = 6;
    private int agility = 3;
    private int stamina = 12;
    private int intellect = 18;
    private int spirit = 3;

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getAgility() {
        return agility;
    }

    @Override
    public int getStamina() {
        return stamina;
    }

    @Override
    public int getIntellect() {
        return intellect;
    }

    @Override
    public int getSpirit() {
        return spirit;
    }

}
