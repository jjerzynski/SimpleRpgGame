package com.hedgehog.games.inventory.ItemHelpers;

import org.jetbrains.annotations.NotNull;

public class ItemSuffix {
    private String suffix;

    public String getSuffix() {
        return suffix = randomSuffix();
    }

    @NotNull
    private String randomSuffix() {
        double rnd = Math.random();
        if (rnd < 0.1) {
            return " of Intellect";
        } else if (rnd < 0.2) {
            return " of Strength";
        } else if (rnd < 0.3) {
            return " of Dexterity";
        } else if (rnd < 0.4) {
            return " of Monkey";
        } else if (rnd < 0.5) {
            return " of Eagle";
        } else if (rnd < 0.6) {
            return " of Bear";
        } else if (rnd < 0.7) {
            return " of Owl";
        } else if (rnd < 0.8) {
            return " of Darkness";
        } else if (rnd < 0.9) {
            return " of Light";
        } else if (rnd <= 1.0) {
            return " of Balance";
        }

        return "";
    }
}
