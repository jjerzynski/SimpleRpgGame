package com.hedgehog.games.rpgmechanics;

import com.hedgehog.games.creatures.CreatureMaker;
import com.hedgehog.games.inventory.ItemMaker;
import org.junit.Assert;
import org.junit.Test;
import com.hedgehog.games.world.World;
import com.hedgehog.games.world.WorldBuilderCustomLevel;

public class SpawnerTest {


    @Test
    public void playerTest_byName(){
        World world = new WorldBuilderCustomLevel(10,10,1).makeLvlWithPattern("floor").build();
        CreatureMaker creatureMaker = new CreatureMaker(world);
        Spawner spawner = new Spawner(creatureMaker, null, world, null, null);

        String expectedResult = "Hero";
        String result = spawner.createPlayer().getName();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void bossTest_byName(){
        World world = new WorldBuilderCustomLevel(10,10,1).makeLvlWithPattern("floor").build();
        CreatureMaker creatureMaker = new CreatureMaker(world);
        Spawner spawner = new Spawner(creatureMaker, null, world, null, null);

        String expectedResult = "Giant Dire Bat";
        String result = spawner.createBoss().getName();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void spawnerDefaultSetup(){
        World world = new WorldBuilderCustomLevel(10,10,1).makeLvlWithPattern("floor").build();
        CreatureMaker creatureMaker = new CreatureMaker(world);
        new Spawner(creatureMaker, null, world, null, null);

        int expectedResult = 2;
        int result = world.getCreaturesList().size();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void spawnerSetupWithMonsters(){
        World world = new WorldBuilderCustomLevel(10,10,1).makeLvlWithPattern("floor").build();
        CreatureMaker creatureMaker = new CreatureMaker(world);
        Spawner spawner = new Spawner(creatureMaker, null, world, null, null);
        spawner.setNoOfCreaturesToSpawnPerLvl(5);
        spawner.spawnCreatures();

        int expectedResult = 9;
        int result = world.getCreaturesList().size();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void spawnerRandomItems(){
        World world = new WorldBuilderCustomLevel(3,3,1).makeLvlWithPattern("floor").build();
        ItemMaker itemMaker = new ItemMaker(world);
        CreatureMaker creatureMaker = new CreatureMaker(world);
        Spawner spawner = new Spawner(creatureMaker, itemMaker, world, null, null);
        spawner.spawnItems();

        int expectedResult = spawner.getNoOfItemsToSpawnPerLvl() *2;
        int result = 0;
        int z = world.getDepth()-1;
        for (int x = 0; x < world.getHeight(); x++) {
            for (int y = 0; y < world.getWidth(); y++){
                if(world.item(x,y,z) != null) {
                    result++;
                }
            }
        }
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void spawnerRandomItems_withSetter(){
        World world = new WorldBuilderCustomLevel(3,3,1).makeLvlWithPattern("floor").build();
        ItemMaker itemMaker = new ItemMaker(world);
        CreatureMaker creatureMaker = new CreatureMaker(world);
        Spawner spawner = new Spawner(creatureMaker, itemMaker, world, null, null);
        spawner.setNoOfItemsToSpawnPerLvl(1);
        spawner.spawnItems();

        int expectedResult = 2;
        int result = 0;
        int z = world.getDepth()-1;
        for (int x = 0; x < world.getHeight(); x++) {
            for (int y = 0; y < world.getWidth(); y++){
                if(world.item(x,y,z) != null) {
                    result++;
                }
            }
        }
        Assert.assertEquals(expectedResult, result);
    }
}