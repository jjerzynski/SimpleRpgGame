package com.hedgehog.games.world;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.inventory.Item;

//World class only responsible for the running of a world and 'living in world' not creating it.
public class World {

    private List<Creature> creaturesList;
    private Tile[][][] tiles;
    private Item[][][] items;
    private int width;
    private int height;
    private int depth;

    public World(Tile[][][] tiles) {

        this.tiles = tiles;
        this.width = tiles.length;
        this.height = tiles[0].length;
        this.depth = tiles[0][0].length;
        this.items = new Item[width][height][depth];
        this.creaturesList = new ArrayList<>();
    }

    public Item item(int x, int y, int z) {
        return items[x][y][z];
    }

    public void update() {
        List<Creature> toUpdate = new ArrayList<>(creaturesList);
        for (Creature c : toUpdate) {
            c.update();
        }
    }

    private boolean checkEmptyLocation(int z) {
        int emptySpaces = 0;
        for (int x = 0; x < getWidth(); x++) {
            for (int y = 0; y < getWidth(); y++) {
                if (tile(x, y, z) == Tile.FLOOR ||
                        tile(x, y, z) == Tile.STAIRSDOWN ||
                        tile(x, y, z) == Tile.TUNNEL)
                    emptySpaces++;
            }
        }

        if (creaturesList.size() < emptySpaces) {
            return true;
        }
        return false;
    }

    public void remove(Creature other) {
        creaturesList.remove(other);
    }

    public void remove(int x, int y, int z) {
        items[x][y][z] = null;
    }

    public Creature creature(int x, int y, int z) {
        for (Creature c : creaturesList) {
            if (c.x == x && c.y == y && c.z == z)
                return c;
        }
        return null;
    }

    public void addAtEmptyLocation(Creature creature, int z) {
        if (checkEmptyLocation(z)) {
            int x;
            int y;

            do {
                x = (int) (Math.random() * width);
                y = (int) (Math.random() * height);
            }
            while (!tile(x, y, z).isFloor() || creature(x, y, z) != null);

            creature.x = x;
            creature.y = y;
            creature.z = z;
            creaturesList.add(creature);
        }
    }

    public void addAtEmptyLocation(Item item, int z) {
        int x;
        int y;

        do {
            x = (int) (Math.random() * width);
            y = (int) (Math.random() * height);
        }
        while (!tile(x, y, z).isFloor() || item(x, y, z) != null);

        items[x][y][z] = item;
    }

    public void addAt(Creature creature, int x, int y, int z) {
        if (tile(x, y, z).isFloor() && creature(x, y, z) == null) {
            creature.x = x;
            creature.y = y;
            creature.z = z;
            creaturesList.add(creature);
        }
    }

    public void addAt(Item item, int x, int y, int z) {
        if (tile(x, y, z).isFloor() && item(x, y, z) == null) {
            items[x][y][z] = item;
        }
    }

    public Tile tile(int x, int y, int z) {
        if (x < 0 || x >= width || y < 0 || y >= height || z < 0 || z >= depth)
            return Tile.BOUNDS;
        else
            return tiles[x][y][z];
    }

    public void dig(int x, int y, int z) {
        if (tile(x, y, z).isDiggable())
            tiles[x][y][z] = Tile.TUNNEL;
    }

    public void corrupt(int x, int y, int z) {

        if (tile(x, y, z).isFloor()) {
            tiles[x][y][z] = Tile.CORRUPTED_GROUND;
        } else if (tile(x, y, z).isDiggable()) {
            tiles[x][y][z] = Tile.CORRUPTED_WALL;
        }
    }

    public char symbol(int x, int y, int z) {
        Creature creature = creature(x, y, z);

        if (creature != null)
            return creature.getSymbol();

        if (item(x, y, z) != null)
            return item(x, y, z).getSymbol();

        return tile(x, y, z).getSymbol();
    }

    public Color color(int x, int y, int z) {
        Creature creature = creature(x, y, z);
        if (creature != null)
            return creature.getColor();

        if (item(x, y, z) != null)
            return item(x, y, z).getColor();

        return tile(x, y, z).getColor();
    }

    //return level width
    public int getWidth() {
        return width;
    }

    //return level height
    public int getHeight() {
        return height;
    }

    //return level depth
    public int getDepth() {
        return depth;
    }

    public List<Creature> getCreaturesList() {
        return creaturesList;
    }

}
