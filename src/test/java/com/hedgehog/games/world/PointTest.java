package com.hedgehog.games.world;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class PointTest {
    @Test
    public void equals_test() {
        Point p1 = new Point(1,1,1);
        Point p2 = new Point(1,1,1);
        Point p3 = new Point(1,0,1);
        Point p4 = new Point(0,1,1);
        Point p5 = new Point(1,1,0);

        Assert.assertTrue(p1.equals(p2));
        Assert.assertFalse(p1.equals(p3));
        Assert.assertFalse(p1.equals(p4));
        Assert.assertFalse(p1.equals(p5));

        Assert.assertEquals(p1, p2);
        Assert.assertNotEquals(p1, p3);
        Assert.assertNotEquals(p1, p4);
        Assert.assertNotEquals(p1, p5);
    }

    @Test
    public void hashCode_test() {
        Point p1 = new Point(1,1,1);
        Point p2 = new Point(1,1,1);
        Point p3 = new Point(1,1,0);

        Assert.assertEquals(p1.hashCode(), p2.hashCode());
        Assert.assertNotEquals(p1.hashCode(), p3.hashCode());
    }

    @Test
    public void point_equals_hash_100times(){
        for (int i = 0; i < 100; i++) {
            equals_test();
            hashCode_test();
        }
    }
}