package com.hedgehog.games.inventory;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class InventoryTest {

    @Test(expected = NegativeArraySizeException.class)
    public void inventory_negativeSize() {
        Inventory inventory = new Inventory(-1);
    }

    @Test
    public void inventory_size0() {

        Inventory inventory = new Inventory(0);
        Assert.assertTrue(inventory.getItems().length == 0);
    }

    @Test
    public void inventory_size() throws Exception {
        Inventory inventory = new Inventory(3);
        Assert.assertTrue(inventory.getItems().length == 3);
    }

    @Test
    public void add() throws Exception {
        Inventory inventory = new Inventory(1);
        Item test = new Item.RandomItemBuilder("weapon").build();

        inventory.add(test);

        Assert.assertTrue(inventory.get(0) != null);
        Assert.assertEquals(test, inventory.get(0));
    }

    @Test
    public void remove() throws Exception {
        Inventory inventory = new Inventory(1);
        Item test = new Item.RandomItemBuilder("weapon").build();

        inventory.add(test);
        inventory.remove(test);

        Assert.assertTrue(inventory.get(0) == null);
    }

    @Test
    public void isFull_size1() throws Exception {
        Inventory inventory = new Inventory(1);
        Item test = new Item.RandomItemBuilder("weapon").build();
        inventory.add(test);
        Assert.assertTrue(inventory.isFull());
    }

    @Test
    public void isFull_size0() throws Exception {
        Inventory inventory = new Inventory(0);
        Assert.assertTrue(inventory.isFull());
    }

    @Test
    public void isNotFull_size2() throws Exception {
        Inventory inventory = new Inventory(2);
        Item test = new Item.RandomItemBuilder("weapon").build();
        inventory.add(test);

        Assert.assertTrue(!inventory.isFull());
        Assert.assertEquals(null, inventory.get(1));
    }

    @Test
    public void getItems() throws Exception {
        Inventory inventory = new Inventory(1);
        Item test = new Item.RandomItemBuilder("weapon").build();
        inventory.add(test);
        Assert.assertTrue(inventory.getItems() != null);
    }

    @Test
    public void get() throws Exception {
        Inventory inventory = new Inventory(1);
        Item test = new Item.RandomItemBuilder("weapon").build();
        inventory.add(test);

        Assert.assertEquals(test, inventory.get(0));
    }

    @Test
    public void allTest_1000times() throws Exception {
        for (int i = 0; i < 1000; i++) {
            inventory_size();
            inventory_size0();
            isFull_size0();
            isFull_size1();
            isNotFull_size2();
            add();
            remove();
            get();
            getItems();
        }
    }
}