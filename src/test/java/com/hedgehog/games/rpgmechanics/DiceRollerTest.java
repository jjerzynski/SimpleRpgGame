package com.hedgehog.games.rpgmechanics;

import com.hedgehog.games.rpgmechanics.DiceRoller;
import org.junit.Test;

import static org.junit.Assert.*;

public class DiceRollerTest {

    @Test
    public void diceRoll_0d0() throws Exception {
        int noOfRolls = 0;
        int diceSize = 0;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_1d0() throws Exception {
        int noOfRolls = 1;
        int diceSize = 0;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_0d6() throws Exception {
        int noOfRolls = 0;
        int diceSize = 6;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_1d6() throws Exception {
        int noOfRolls = 1;
        int diceSize = 6;
        boolean expectedResult = true;
        boolean result = false;

        DiceRoller diceRoller = new DiceRoller();

        int rollValue = diceRoller.diceRoll(noOfRolls, diceSize);
        if(rollValue >= 1 && rollValue <= 6) result = true;

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_1d6_10000times() throws Exception {
        int noOfRolls = 1;
        int diceSize = 6;
        boolean expectedResult = true;
        boolean result = true;

        DiceRoller diceRoller = new DiceRoller();
        for (int i = 0; i < 10000; i++) {
            int rollValue = diceRoller.diceRoll(noOfRolls, diceSize);
            if (rollValue < 1){
                result = false;
                break;
            }
            if (rollValue > 6){
                result = false;
                break;
            }
        }

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_5d10() throws Exception {
        int noOfRolls = 5;
        int diceSize = 10;
        boolean expectedResult = true;
        boolean result = false;

        DiceRoller diceRoller = new DiceRoller();

        int rollValue = diceRoller.diceRoll(noOfRolls, diceSize);
        if(rollValue >= 5 && rollValue <= 50) result = true;

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_5d1add1() throws Exception {
        int noOfRolls = 5;
        int diceSize = 1;
        int bonusPerRoll = 1;

        DiceRoller diceRoller = new DiceRoller();

        assertEquals(10, diceRoller.diceRoll(noOfRolls, diceSize, bonusPerRoll));
    }

    @Test
    public void diceRoll_5d10_10000times() throws Exception {
        int noOfRolls = 5;
        int diceSize = 10;
        boolean expectedResult = true;
        boolean result = true;

        DiceRoller diceRoller = new DiceRoller();
        for (int i = 0; i < 10000; i++) {
            int rollValue = diceRoller.diceRoll(noOfRolls, diceSize);
            if (rollValue < 5){
                result = false;
                break;
            }
            if (rollValue > 50){
                result = false;
                break;
            }
        }

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_0d0add0() throws Exception {
        int noOfRolls = 0;
        int diceSize = 0;
        int bonus = 0;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize, bonus);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_1d0add0() throws Exception {
        int noOfRolls = 1;
        int diceSize = 0;
        int bonus = 0;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize, bonus);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_0d6add0() throws Exception {
        int noOfRolls = 0;
        int diceSize = 6;
        int bonus = 0;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize, bonus);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_0d0add10() throws Exception {
        int noOfRolls = 0;
        int diceSize = 0;
        int bonus = 10;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize, bonus);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_1d0add10() throws Exception {
        int noOfRolls = 1;
        int diceSize = 0;
        int bonus = 10;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize, bonus);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_0d6add10() throws Exception {
        int noOfRolls = 0;
        int diceSize = 6;
        int bonus = 10;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize, bonus);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_1d6add10() throws Exception {
        int noOfRolls = 1;
        int diceSize = 6;
        int bonus = 10;
        boolean expectedResult = true;
        boolean result = true;

        DiceRoller diceRoller = new DiceRoller();

        int rollResult = diceRoller.diceRoll(noOfRolls, diceSize, bonus);
        if(rollResult < 11) result = false;
        if(rollResult > 16) result = false;

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_1d6add10_10000times() throws Exception {
        int noOfRolls = 1;
        int diceSize = 6;
        int bonus = 10;
        boolean expectedResult = true;
        boolean result = true;

        DiceRoller diceRoller = new DiceRoller();

        for (int i = 0; i < 10000; i++) {
            int rollResult = diceRoller.diceRoll(noOfRolls, diceSize, bonus);
            if(rollResult < 11) {
                result = false;
                break;
            }
            if(rollResult > 16) {
                result = false;
                break;
            }
        }

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_1d6add_minus10_10000times() throws Exception {
        int noOfRolls = 1;
        int diceSize = 6;
        int bonus = -10;
        boolean expectedResult = true;
        boolean result = true;

        DiceRoller diceRoller = new DiceRoller();

        for (int i = 0; i < 10000; i++) {
            int rollResult = diceRoller.diceRoll(noOfRolls, diceSize, bonus);
            if(rollResult != 0){
                result = false;
                break;
            }
        }
        assertEquals(expectedResult, result);
    }
}