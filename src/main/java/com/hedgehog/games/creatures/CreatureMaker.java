package com.hedgehog.games.creatures;

import com.hedgehog.games.creatures.animals.BatAI;
import com.hedgehog.games.creatures.corruptor.CrimsonColonyAI;
import com.hedgehog.games.creatures.fungus.*;
import com.hedgehog.games.creatures.player.*;
import com.hedgehog.games.rpgmechanics.FieldOfView;
import com.hedgehog.games.sounds.SoundFactory;
import com.hedgehog.games.world.World;
import java.awt.Color;
import java.util.List;

//This is not a Factory in meaning of Creation Patterns. Why? Because this is simplest and most effective way without
//making GameWindow or any other class to complicated and unreadable.

public class CreatureMaker {
    private World world;
    private SoundFactory sound = new SoundFactory();

    public CreatureMaker(World world){
        this.world = world;
    }

    //This method create a player's Hero and place him or hes on empty tile (non-wall, non-creature)
    public Creature newPlayer(List<String> messages, FieldOfView fieldOfView){

        Creature player = new Creature.CreatureBuilder(world, "Hero", '@',
                1, 1).withHpBonus(99)
                .withNoOfAttDice(2).withAttDice(6).withArmorClass(5)
                .withInventory(10).build();

        world.addAtEmptyLocation(player, 0);
        sound.playerSounds(player);
        new PlayerAI(player, messages, fieldOfView);
        return player;
    }

    public Creature newCorruptor(int depth, FieldOfView fieldOfView){

        Creature corruptor = new Creature.CreatureBuilder(world, "Corruptor", 'C', 5,5)
                .withHpBonus(10).withArmorClass(5).withColor(new Color(75, 0, 130)).build();

        world.addAtEmptyLocation(corruptor, depth);
        new CrimsonColonyAI(corruptor, 40, depth, fieldOfView);
        return corruptor;
    }

    public Creature newFungus(int depth){
        Creature fungus = new Creature.CreatureBuilder(world, "Red Fungus", 'F',
                2,8).withHpBonus(8)
                .withNoOfAttDice(1).withAttDice(8).withAttBonus(3).withArmorClass(5)
                .withColor(new Color(220, 20, 60)).build();
        world.addAtEmptyLocation(fungus,depth);
        sound.fungusSounds(fungus);
        new FungusAI(fungus, this);
        return fungus;
    }

    public Creature newSmallFungus(int depth){
        Creature smallFungus = new Creature.CreatureBuilder(world, "Small Red Fungus", 'f',
                2,6).withHpBonus(6)
                .withNoOfAttDice(1).withAttDice(8).withAttBonus(1).withArmorClass(3)
                .withColor(new Color(220, 20, 60)).build();

        world.addAtEmptyLocation(smallFungus, depth);
        sound.fungusSounds(smallFungus);
        new SmallFungusAI(smallFungus, this);
        return smallFungus;
    }

    //Child of FungusChild, cant spread and attack (path blocker)
    public Creature newTinyFungus(int depth){
        Creature tinyFungus = new Creature.CreatureBuilder
                (world, "Tiny fungus", 'f', 2,4).withHpBonus(1)
                .withColor(new Color(139, 0, 0)).build();

        world.addAtEmptyLocation(tinyFungus, depth);
        sound.fungusSounds(tinyFungus);
        new TinyFungusAI(tinyFungus);
        return tinyFungus;
    }

    public Creature newCaveBat(int depth){
        Creature caveBat = new Creature.CreatureBuilder(world, "Cave Bat", 'B',
                2, 8)
                .withNoOfAttDice(1).withAttDice(8).withAttBonus(1).withArmorClass(3)
                .withColor(new Color(139, 69, 19)).build();

        world.addAtEmptyLocation(caveBat, depth);
        sound.batSounds(caveBat);
        new BatAI(caveBat);
        return caveBat;
    }

    public Creature newDireBat(int depth){
        Creature direBat = new Creature.CreatureBuilder(world, "Dire Bat", 'B',
                4, 10)
                .withNoOfAttDice(1).withAttDice(6).withAttBonus(6).withArmorClass(5)
                .withColor(new Color(160, 82, 45)).build();

        world.addAtEmptyLocation(direBat, depth);
        sound.batSounds(direBat);
        new BatAI(direBat);
        return direBat;
    }

    public Creature newGiantDireBat(int depth){
        Creature giantDireBat = new Creature.CreatureBuilder(world, "Giant Dire Bat", 'B',
                5, 8).withHpBonus(4)
                .withNoOfAttDice(2).withAttDice(8).withHpBonus(2).withArmorClass(10)
                .withColor(new Color(210, 105, 30)).withInventory(1).build();

        world.addAtEmptyLocation(giantDireBat, depth);
        sound.batSounds(giantDireBat);
        new BatAI(giantDireBat);
        return giantDireBat;
    }
}