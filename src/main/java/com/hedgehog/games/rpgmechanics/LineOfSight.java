package com.hedgehog.games.rpgmechanics;

import com.hedgehog.games.world.Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//This class creates line of sight for our com.hedgehog.games.creatures
//Long story short: how far creature see
public class LineOfSight implements Iterable<Point>{

    private List<Point> pointList;

    public LineOfSight(int x1, int y1, int x2, int y2){

        pointList = new ArrayList<>();
        LineOfSightAlgorithm(x1, y1, x2, y2);
    }

    private void LineOfSightAlgorithm(int x1, int y1, int x2, int y2) {
        int dx = Math.abs(x2 - x1);
        int dy = Math.abs(y2 - y1);
        int sx = x1 < x2 ? 1 : -1;
        int sy = y1 < y2 ? 1 : -1;
        int error = dx - dy;

        drawLine(x1, y1, x2, y2, dx, dy, sx, sy, error);
    }

    private void drawLine(int x1, int y1, int x2, int y2, int dx, int dy, int sx, int sy, int error) {
        while(true){
            pointList.add(new Point(x1, y1,0));

            if(x1 == x2 && y1 == y2) break;

            int error2 = error * 2;

            if(error2 > -dx){
                error -= dy;
                x1 += sx;
            }
            if (error2 < dx) {
                error += dx;
                y1 += sy;
            }
        }
    }

    public Iterator<Point> iterator() {
        return pointList.iterator();
    }
}
