package com.hedgehog.games.inventory;

import com.hedgehog.games.inventory.Item;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

public class ItemBuilderTest {

    @Test
    public void itemBuilder_test(){
        Item item = new Item.ItemBuilder('t', "test")
                .withAttValue(1).withDefValue(1).withColor(Color.RED).build();

        Assert.assertEquals(1, item.getAttackValue());
        Assert.assertEquals(1, item.getDefenseValue());
        Assert.assertEquals(Color.red, item.getColor());
    }
}
