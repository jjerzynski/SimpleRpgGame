package com.hedgehog.games.interfaces;

import java.awt.event.KeyEvent;

import asciiTable.*;

//Each screen displays output on our AsciiTable and responds to user input
//this can be represented by this simple ApplicationWindow interface.
public interface ApplicationWindow {

    void displayOutput(AsciiTable console);
    ApplicationWindow respondToUserInput(KeyEvent e);

}
