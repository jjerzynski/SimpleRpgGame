package com.hedgehog.games.creatures.creatureHelpers.raceattributes;

public class AnimalAttributes extends CreatureAttributes{
    private int strength = 8;
    private int agility = 8;
    private int stamina = 8;
    private int intellect = 5;
    private int spirit = 3;

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getAgility() {
        return agility;
    }

    @Override
    public int getStamina() {
        return stamina;
    }

    @Override
    public int getIntellect() {
        return intellect;
    }

    @Override
    public int getSpirit() {
        return spirit;
    }

}
