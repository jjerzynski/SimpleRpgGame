package com.hedgehog.games.rpgmechanics;

import com.hedgehog.games.world.Point;
import com.hedgehog.games.world.Tile;
import com.hedgehog.games.world.World;


//This class creates field of view
//Long story short: with this class player cant see trough walls and other world obstacles
public class FieldOfView {

    private World world;
    private int depth;

    private boolean[][] visible;
    private Tile[][][] tiles;

    public FieldOfView(World world){
        this.world = world;
        this.visible = new boolean[world.getWidth()][world.getHeight()];
        this.tiles = new Tile[world.getWidth()][world.getHeight()][world.getDepth()];
        createFog(world);
    }

    private void createFog(World world) {
        for (int x = 0; x < world.getWidth(); x++) {
            for (int y = 0; y < world.getHeight(); y++) {
                for (int z = 0; z < world.getDepth(); z++) {
                    tiles[x][y][z] = Tile.UNKNOWN;
                }
            }
        }
    }

    public void update(int worldX, int worldY, int worldZ, int radius){
        depth = worldZ;
        visible = new boolean[world.getWidth()][world.getHeight()];
        for (int x = -radius; x < radius; x++) {
            for (int y = -radius; y < radius; y++) {
                checkVisibility(worldX, worldY, worldZ, radius, x, y);
            }
        }
    }

    private void checkVisibility(int worldX, int worldY, int worldZ, int radius, int x, int y) {
        if(x * x + y * y > radius * radius)
            return;

        if(worldX + x < 0 || worldX + x >= world.getWidth() ||
                worldY + y < 0 || worldY + y >= world.getHeight())
            return;

        setVisible(worldX, worldY, worldZ, x, y);
    }

    private void setVisible(int worldX, int worldY, int worldZ, int x, int y) {
        for (Point point : new LineOfSight(worldX, worldY, worldX + x, worldY + y)) {
            Tile tile = world.tile(point.x, point.y, worldZ);
            visible[point.x][point.y] = true;
            tiles[point.x][point.y][worldZ] = tile;

            if(!tile.isFloor())
                break;
        }
    }

    public boolean isVisible(int x, int y, int z){
        return z == depth && x >= 0 && y >= 0 && x < visible.length && y < visible[0].length && visible[x][y];
    }

    public Tile tile(int x, int y, int z){
        return tiles[x][y][z];
    }

}
