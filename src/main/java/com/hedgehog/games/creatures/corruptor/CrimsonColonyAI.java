package com.hedgehog.games.creatures.corruptor;

import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.creatures.CreatureAI;
import com.hedgehog.games.rpgmechanics.FieldOfView;


public class CrimsonColonyAI extends CreatureAI {
    private int noOfSpreads;
    private int corruptRadius = 5;
    private double corruptSpreadChange = 0.05;
    private int depth;
    private FieldOfView fieldOfView;

    public CrimsonColonyAI
            (Creature creature, int noOfSpreads, int depth, FieldOfView fieldOfView) {
        super(creature);
        this.noOfSpreads = noOfSpreads;
        this.depth = depth;
        this.fieldOfView = fieldOfView;
    }

    public boolean canSee(int worldX, int worldY, int worldZ){
        return fieldOfView.isVisible(worldX, worldY, worldZ);
    }

    public void onUpdate(){

        double rnd = Math.random();
        if(noOfSpreads > 0 && rnd < corruptSpreadChange) {
            corrupt();
            noOfSpreads--;
        }
    }
    
    private void corrupt(){
        int x;
        int y;
        int z = depth;

        do {
            x = randomizeCoordinate(-corruptRadius, corruptRadius);
            y = randomizeCoordinate(-corruptRadius, corruptRadius);
        } while(corruptRadius * corruptRadius < x * x + y * y);

        int corruptX = creature.x + x;
        int corruptY = creature.y + y;

        if(!creature.canCorrupt(corruptX, corruptY, z))
            return;
        creature.corrupt(corruptX,corruptY,z);

    }
}
