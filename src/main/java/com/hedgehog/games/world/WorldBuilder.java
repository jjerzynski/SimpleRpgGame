package com.hedgehog.games.world;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

////WorldBuilder class only responsible for creating world with generic method.
public class WorldBuilder {

    private int width;
    private int height;
    private int depth;
    private Tile[][][] tiles;
    private int[][][] regions;
    private int nextRegion;


    public WorldBuilder(int width, int height, int depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.tiles = new Tile[width][height][depth];
        this.nextRegion = 1;
    }

    public World build() {
        return new World(tiles);
    }

    private WorldBuilder randomizeTiles() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    tiles[x][y][z] = Math.random() < 0.55 ? Tile.FLOOR : Tile.WALL;
                }
            }
        }
        return this;
    }

    private WorldBuilder smoothWorld(int times) {
        Tile[][][] smoothedTiles = new Tile[width][height][depth];
        if(times == 0){
            return this;
        }
        for (int i = 0; i < times; i++) {
            smoothLevel(smoothedTiles);
        }
        return this;
    }

    private void smoothLevel(Tile[][][] smoothedTiles) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    smoothTile(smoothedTiles, x, y, z);
                }
            }
        }
        tiles = smoothedTiles;
    }

    private void smoothTile(Tile[][][] smoothedTiles, int x, int y, int z) {
        int floors = 0;
        int walls = 0;

        for (int xx = -1; xx < 2; xx++) {
            for (int yy = -1; yy < 2; yy++) {
                if (x + xx < 0 || x + xx >= width || y + yy < 0 || y + yy >= height)
                    continue;
                if (tiles[x + xx][y + yy][z] == Tile.FLOOR) {
                    floors++;
                } else {
                    walls++;
                }
            }
        }
        smoothedTiles[x][y][z] = floors > walls ? Tile.FLOOR : Tile.WALL;
    }

    private WorldBuilder createRegions() {
        regions = new int[width][height][depth];

        for (int z = 0; z < depth; z++) {
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    validateRegion(z, x, y);
                }
            }
        }
        return this;
    }

    private void validateRegion(int z, int x, int y) {
        if (tiles[x][y][z] != Tile.WALL && regions[x][y][z] == 0) {
            int size = fillRegion(nextRegion++, x, y, z);

            if (size < 25) {
                removeRegion(nextRegion - 1, z);
            }
        }
    }

    private void removeRegion(int region, int z) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (regions[x][y][z] == region) {
                    regions[x][y][z] = 0;
                    tiles[x][y][z] = Tile.WALL;
                }
            }
        }
    }

    private int fillRegion(int region, int x, int y, int z) {
        int size = 1;
        ArrayList<Point> open = new ArrayList<>();
        open.add(new Point(x, y, z));
        regions[x][y][z] = region;

        while (!open.isEmpty()) {
            Point p = open.remove(0);
            for (Point neighbor : p.neighborsOfPoint()) {
                if (neighbor.x < 0 || neighbor.y < 0 || neighbor.x >= width || neighbor.y >= height)
                    continue;

                if (regions[neighbor.x][neighbor.y][neighbor.z] > 0
                        || tiles[neighbor.x][neighbor.y][neighbor.z] == Tile.WALL)
                    continue;

                size++;
                regions[neighbor.x][neighbor.y][neighbor.z] = region;
                open.add(neighbor);
            }
        }
        return size;
    }

    private WorldBuilder connectRegions() {
        for (int z = 0; z < depth - 1; z++) {
            connectRegionsDown(z);
        }
        return this;
    }

    private void connectRegionsDown(int z) {
        List<String> connected = new ArrayList<>();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                String region = regions[x][y][z] + "," + regions[x][y][z + 1];
                if (tiles[x][y][z] == Tile.FLOOR
                        && tiles[x][y][z + 1] == Tile.FLOOR
                        && !connected.contains(region)) {
                    connected.add(region);
                    connectRegionsDownWithStairs(z, regions[x][y][z], regions[x][y][z + 1]);
                }
            }
        }
    }

    private void connectRegionsDownWithStairs(int z, int r1, int r2) {
        List<Point> candidates = findRegionOverlaps(z, r1, r2);
        Point p = candidates.remove(0);
        tiles[p.x][p.y][z] = Tile.STAIRSDOWN;
        tiles[p.x][p.y][z + 1] = Tile.STAIRSUP;
    }

    private List<Point> findRegionOverlaps(int z, int r1, int r2) {
        ArrayList<Point> candidates = new ArrayList<>();

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (tiles[x][y][z] == Tile.FLOOR
                        && tiles[x][y][z + 1] == Tile.FLOOR
                        && regions[x][y][z] == r1
                        && regions[x][y][z + 1] == r2) {
                    candidates.add(new Point(x, y, z));
                }
            }
        }
        Collections.shuffle(candidates);
        return candidates;
    }

    public WorldBuilder makeCaves(int smoothLevel) {
        return randomizeTiles()
                .smoothWorld(smoothLevel)
                .createRegions()
                .connectRegions();
    }

    public Tile getTile(int x, int y, int z) {
        return tiles[x][y][z];
    }
}
