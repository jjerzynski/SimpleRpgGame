package com.hedgehog.games.inventory;

import com.hedgehog.games.inventory.ItemHelpers.ItemSymbol;
import com.hedgehog.games.world.World;

public class ItemMaker {
    private World world;
    private ItemSymbol itemSymbol;

    public ItemMaker(World world){
        this.world = world;
        this.itemSymbol = new ItemSymbol();
    }

    public Item newRandomWeapon(int depth){
        Item randomWeapon = new Item.RandomItemBuilder("weapon").build();
        world.addAtEmptyLocation(randomWeapon, depth);
        return randomWeapon;
    }

    public Item newRandomArmor(int depth){
        Item randomArmor = new Item.RandomItemBuilder("armor").build();
        world.addAtEmptyLocation(randomArmor, depth);
        return randomArmor;
    }
}