package com.hedgehog.games.world;

import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.inventory.Item;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

public class WorldTest {

    @Test
    public void world_getSymbol(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1, 1)
                .withColor(Color.RED).build();
        Item item = new Item.ItemBuilder('i', "test").withColor(Color.RED).build();

        world.addAt(creature, 0,0,0);
        world.addAt(item,1,1,0);

        Assert.assertEquals('t', world.symbol(0,0,0));
        Assert.assertEquals('i', world.symbol(1,1,0));
        Assert.assertEquals(world.tile(2,2,0).getSymbol(), world.symbol(2,2,0));
    }

    @Test
    public void world_getColor(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1, 1)
                .withColor(Color.RED).build();
        Item item = new Item.ItemBuilder('i', "test").withColor(Color.RED).build();

        world.addAt(creature, 0,0,0);
        world.addAt(item,1,1,0);

        Assert.assertEquals(Color.red, world.color(0,0,0));
        Assert.assertEquals(Color.red, world.color(1,1,0));
        Assert.assertEquals(world.tile(2,2,0).getColor(), world.color(2,2,0));
    }

    @Test
    public void world_object_100times(){
        for (int i = 0; i < 100; i++) {
            world_getColor();
            world_getSymbol();

        }
    }

    /**
     * ADD/REMOVE ITEM/CREATURE TESTS IN GENERIC WORLD
     */

    @Test
    public void creature_addAtEmptyLocation() {
        World world = new WorldBuilder(20, 20, 1).makeCaves(10).build();
        Creature creature = new Creature.CreatureBuilder(world, "Test", 't', 1, 1).build();
        world.addAtEmptyLocation(creature, 0);
        int expectedResult = 1;

        int result = world.getCreaturesList().size();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void item_addAtEmptyLocationRandomWeapon() {
        World world = new WorldBuilder(20, 20, 1).makeCaves(10).build();
        Item item = new Item.RandomItemBuilder("weapon").build();
        world.addAtEmptyLocation(item, 0);
        boolean expectedResult = true;

        boolean result = false;

        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (world.item(i, j, 0) != null) result = true;
            }
        }

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void item_addAtEmptyLocationRandomArmor() {
        World world = new WorldBuilder(20, 20, 1).makeCaves(10).build();
        Item item = new Item.RandomItemBuilder("armor").build();
        world.addAtEmptyLocation(item, 0);
        boolean expectedResult = true;

        boolean result = false;

        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (world.item(i, j, 0) != null) result = true;
            }
        }

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_removeFromWorld () {
        World world = new WorldBuilder(20, 20, 1).makeCaves(10).build();
        Creature creature = new Creature.CreatureBuilder(world, "Test", 't', 1, 1).build();
        world.addAtEmptyLocation(creature,0);
        world.remove(creature);
        int expectedResult = 0;

        int result = world.getCreaturesList().size();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void item_removeFromWorld() {
        World world = new WorldBuilder(20, 20, 1).makeCaves(10).build();
        Item item = new Item.RandomItemBuilder("weapon").build();
        world.addAtEmptyLocation(item, 0);
        boolean expectedResult = true;
        boolean result = true;
        int itemx = 0;
        int itemy = 0;
        int itemz = 0;

        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (world.item(i, j, 0) != null) {
                    itemx = i;
                    itemy = j;
                }
            }
        }

        world.remove(itemx, itemy, itemz);

        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (world.item(i, j, 0) != null) {
                    result = false;
                    break;

                }
            }
        }
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void add_remove_item_Creature_tests_1000times(){
        for (int i = 0; i < 1000; i++) {
            item_addAtEmptyLocationRandomArmor();
            item_addAtEmptyLocationRandomWeapon();
            item_removeFromWorld();
            creature_addAtEmptyLocation();
            creature_removeFromWorld();
        }
    }

    /**
     * WORLD TILES TESTS
     */


    @Test
    public void worldIsFloor(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < world.getWidth(); i++) {
            for (int j = 0; j < world.getHeight(); j++) {
                if(world.tile(i,j,0) == Tile.FLOOR)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void worldIsWall(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("wall").build();
        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < world.getWidth(); i++) {
            for (int j = 0; j < world.getHeight(); j++) {
                if(world.tile(i,j,0) == Tile.WALL)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void onlyOneFloor(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onefloor").build();
        int expectedResult = 1;
        int result = 0;

        for (int i = 0; i < world.getWidth(); i++) {
            for (int j = 0; j < world.getHeight(); j++) {
                if(world.tile(i,j,0) == Tile.FLOOR)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void onlyOneWall(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall").build();
        int expectedResult = 1;
        int result = 0;

        for (int i = 0; i < world.getWidth(); i++) {
            for (int j = 0; j < world.getHeight(); j++) {
                if(world.tile(i,j,0) == Tile.WALL)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void smoothLevelTest_floors(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall")
                .smoothWorldTest(1).build();

        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < world.getWidth(); i++) {
            for (int j = 0; j < world.getHeight(); j++) {
                if(world.tile(i,j,0) == Tile.FLOOR)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);

    }

    @Test
    public void smoothLevelTest_walls(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onefloor")
                .smoothWorldTest(1).build();

        int expectedResult = 9;
        int result = 0;

        for (int i = 0; i < world.getWidth(); i++) {
            for (int j = 0; j < world.getHeight(); j++) {
                if(world.tile(i,j,0) == Tile.WALL)
                    result++;
            }
        }

        Assert.assertEquals(expectedResult,result);

    }

    @Test
    public void allTest_1000times(){
        for (int i = 0; i < 1000; i++) {
            worldIsFloor();
            worldIsWall();
            onlyOneFloor();
            onlyOneWall();
            smoothLevelTest_floors();
            smoothLevelTest_walls();
        }
    }

}