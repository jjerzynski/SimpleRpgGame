package com.hedgehog.games.interfaces;

public class AppVersion {
    private String version;
    private String releaseNumber = "0.1.8.2";
    private String versionName = "-Public";


    public AppVersion() {
        this.version = releaseNumber + versionName;
    }

    public String getVersion() {
        return version;
    }
}
