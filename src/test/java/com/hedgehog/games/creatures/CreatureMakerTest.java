package com.hedgehog.games.creatures;
import org.junit.Assert;
import org.junit.Test;
import com.hedgehog.games.world.World;
import com.hedgehog.games.world.WorldBuilderCustomLevel;

public class CreatureMakerTest {

    @Test
    public void newPlayer(){
        World worldFloor = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        CreatureMaker makerF = new CreatureMaker(worldFloor);
        makerF.newPlayer(null, null);

        int expectedResult = 1;
        int result = worldFloor.getCreaturesList().size();

        Assert.assertEquals(expectedResult,result);

    }

    @Test
    public void newFungus_floor(){
        World worldFloor = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        CreatureMaker makerF = new CreatureMaker(worldFloor);
        makerF.newFungus(0);

        int expectedResult = 1;
        int result = worldFloor.getCreaturesList().size();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void newFungus_noFloor(){
        World worldFloor = new WorldBuilderCustomLevel(1).makeLvlWithPattern("wall").build();
        CreatureMaker makerF = new CreatureMaker(worldFloor);
        makerF.newFungus(0);

        int expectedResult = 0;
        int result = worldFloor.getCreaturesList().size();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void newFungus_oneFloor_moreMonsters(){
        World worldFloor = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onefloor").build();
        CreatureMaker makerF = new CreatureMaker(worldFloor);
        makerF.newFungus(0);
        makerF.newFungus(0);

        int expectedResult = 1;
        int result = worldFloor.getCreaturesList().size();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void newBat_floor(){
        World worldFloor = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        CreatureMaker makerF = new CreatureMaker(worldFloor);
        makerF.newCaveBat(0);

        int expectedResult = 1;
        int result = worldFloor.getCreaturesList().size();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void newBat_noFloor(){
        World worldFloor = new WorldBuilderCustomLevel(1).makeLvlWithPattern("wall").build();
        CreatureMaker makerF = new CreatureMaker(worldFloor);
        makerF.newCaveBat(0);

        int expectedResult = 0;
        int result = worldFloor.getCreaturesList().size();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void newBat_oneFloor_moreMonsters(){
        World worldFloor = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onefloor").build();
        CreatureMaker makerF = new CreatureMaker(worldFloor);
        makerF.newCaveBat(0);
        makerF.newCaveBat(0);
        makerF.newCaveBat(0);
        makerF.newCaveBat(0);

        int expectedResult = 1;
        int result = worldFloor.getCreaturesList().size();

        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void allTest_1000times(){
        for (int i = 0; i < 1000; i++) {
            newBat_floor();
            newBat_noFloor();
            newBat_oneFloor_moreMonsters();
            newFungus_floor();
            newFungus_noFloor();
            newFungus_oneFloor_moreMonsters();
            newPlayer();

        }
    }


}