package com.hedgehog.games.gui;

import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.interfaces.ApplicationWindow;
import com.hedgehog.games.interfaces.InventoryWindow;
import com.hedgehog.games.inventory.Item;

public class BackpackWindow extends InventoryWindow {

    public BackpackWindow(Creature player, GameWindow gameWindow) {
        super(player, gameWindow);
    }

    @Override
    protected String getVerb() {
        return "remove";
    }

    @Override
    protected boolean isAcceptable(Item item) {
        return true;
    }

    @Override
    protected ApplicationWindow use(Item item) {
        player.removeItemFromEq(item);
        return null;
    }
}
