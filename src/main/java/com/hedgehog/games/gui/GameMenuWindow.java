package com.hedgehog.games.gui;

import asciiTable.AsciiTable;
import colors.*;
import com.hedgehog.games.interfaces.AppVersion;
import com.hedgehog.games.interfaces.ApplicationWindow;
import java.awt.*;
import java.awt.event.KeyEvent;

//First screen players will see. This screen shows some info and sets us 'play'

public class GameMenuWindow implements ApplicationWindow {

    AppVersion version = new AppVersion();
    @Override
    public void displayOutput(AsciiTable console) {

        versionInfo(console);
        movementInfo(console);
        actionsInfo(console);
        tilesInfo(console);
        monstersInfo(console);
        itemsInfo(console);

        console.writeCenter(" -- press [enter] to start --", 22, Green.darkLime);
    }

    private void versionInfo(AsciiTable console) {
        console.write("Current ver.:", 1,1, Blue.brightBlue);
        console.write(version.getVersion(), 15, 1, Green.darkLime);
    }

    private void movementInfo(AsciiTable console) {

        console.write("Movement:",3,5, Green.darkLime);
        console.write("NumPad", 17, 5);
        console.write("- UP:         num 8",3,6);
        console.write("- DOWN:       num 2",3,7);
        console.write("- LEFT:       num 4",3,8);
        console.write("- RIGHT:      num 6",3,9);
        console.write("- - - - - - - - - -",3,10);
        console.write("- UP-LEFT:    num 7",3,11);
        console.write("- UP-RIGHT:   num 9",3,12);
        console.write("- DOWN-LEFT:  num 1",3,13);
        console.write("- DOWN-RIGHT: num 3",3,14);
        console.write("- HOLD POS.:  num 5",3,15);
    }

    private void itemsInfo(AsciiTable console){
        console.write("Items:",3,17, Green.darkLime);
        console.write("in progress",10,17, Color.gray);
        console.write("white and grey \"glyphs\" (symbols) ",3,18);
        console.write("different than monsters", 3, 19);

    }

    private void actionsInfo(AsciiTable console) {
        console.write("Actions:", 30, 5, Green.darkLime);
        console.write("- Go down:  .  (<)",30,6);
        console.write("- Go up:    ,  (>)",30,7);
        console.write("- Pickup:   p", 30,8);
        console.write("- Backpack: b", 30, 9);
    }

    private void tilesInfo(AsciiTable console) {
        console.write("Dungeon tiles:  ", 30, 11, Green.darkLime);
        console.write("- Cave floor:   " + (char)250, 30, 12);
        console.write("- Cave wall:    " + (char)219, 30, 13);
        console.write("- Tunnel:       " + (char)176, 30, 14);
        console.write("- Stairs down:  >", 30, 15);
        console.write("- Stairs up::   <", 30, 16);
    }

    private void monstersInfo(AsciiTable console){
        console.write("Monsters:", 57, 5, Green.darkLime);
        console.write("(examples)", 67, 5, Grey.gray);
        console.write("- Fungus:", 57, 6);
        console.write("F", 67, 6, Red.crimson);
        console.write("(stationary, aggressive, can spreads)",57,7);
        console.write("- Bat:",57,8);
        console.write("B",64,8, Brown.darkBrown);
        console.write("(wandering, aggressive, 2Att/r)",57,9);
        console.write("- Corruptor:",57,10);
        console.write("C",70,10, Purple.indygo);
        console.write("(stationary, spread corruption)",57,11);
    }

    @Override
    public ApplicationWindow respondToUserInput(KeyEvent key) {
        return key.getKeyCode() == KeyEvent.VK_ENTER ? new GameWindow() : this;

    }
}
