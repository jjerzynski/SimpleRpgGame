package com.hedgehog.games.world;

public class WorldBuilderCustomLevel {
    private int width;
    private int height;
    private int depth;
    public Tile[][][] tiles;

    public WorldBuilderCustomLevel(int noOfLevels) {
        this.width = 3;
        this.height = 3;
        this.depth = noOfLevels;
        this.tiles = new Tile[width][height][depth];
    }

    public WorldBuilderCustomLevel(int width, int height, int depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.tiles = new Tile[width][height][depth];
    }

    public World build() {
        return new World(tiles);
    }

    private WorldBuilderCustomLevel makeFloor(String pattern) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    designPattern(pattern, x, y, z);
                }
            }
        }
        return this;
    }

    private void designPattern(String pattern, int x, int y, int z) {
        switch(pattern) {
            case "floor":
                tiles[x][y][z] = Tile.FLOOR;
                break;
            case "wall":
                tiles[x][y][z] = Tile.WALL;
                break;
            case "onewall":
                if (x == 1 && y == 1) {
                    tiles[x][y][z] = Tile.WALL;
                } else {
                    tiles[x][y][z] = Tile.FLOOR;
                }
                break;
            case "onefloor":
                if (x == 1 && y == 1) {
                    tiles[x][y][z] = Tile.FLOOR;
                } else {
                    tiles[x][y][z] = Tile.WALL;
                } break;
            case "floorlinemiddle":
                if(x == 1) {
                    tiles[x][y][z] = Tile.FLOOR;
                } else {
                    tiles[x][y][z] = Tile.WALL;
                }
                break;
            case "walllinemiddle":
                if(x == 1) {
                    tiles[x][y][z] = Tile.WALL;
                } else {
                    tiles[x][y][z] = Tile.FLOOR;
                }
                break;
            case "floorlineright":
                if(y == 2) {
                    tiles[x][y][z] = Tile.FLOOR;
                } else {
                    tiles[x][y][z] = Tile.WALL;
                }
                break;
            case "walllineright":
                if(y == 2) {
                    tiles[x][y][z] = Tile.WALL;
                } else {
                    tiles[x][y][z] = Tile.FLOOR;
                }
                break;
            case "oryginal":
                tiles[x][y][z] = Math.random() < 0.55 ? Tile.FLOOR : Tile.WALL;
                break;
        }
    }

    private WorldBuilderCustomLevel makeWalls() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    tiles[x][y][z] = Tile.WALL;
                }
            }
        }
        return this;
    }

    private WorldBuilderCustomLevel smoothWorld(int times) {
        Tile[][][] smoothedTiles = new Tile[width][height][depth];
        if(times == 0){
            return this;
        }
        for (int i = 0; i < times; i++) { //Smooth cave loop;
            smoothLevel(smoothedTiles);
        }
        return this;
    }

    private void smoothLevel(Tile[][][] smoothedTiles) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    smoothTile(smoothedTiles, x, y, z);
                }
            }
        }
        tiles = smoothedTiles;
    }

    private void smoothTile(Tile[][][] smoothedTiles, int x, int y, int z) {
        int floors = 0;
        int walls = 0;

        for (int xx = -1; xx < 2; xx++) {
            for (int yy = -1; yy < 2; yy++) {
                if (x + xx < 0 || x + xx >= width || y + yy < 0 || y + yy >= height)
                    continue;
                if (tiles[x + xx][y + yy][z] == Tile.FLOOR) {
                    floors++;
                } else {
                    walls++;
                }
            }
        }
        smoothedTiles[x][y][z] = floors > walls ? Tile.FLOOR : Tile.WALL;
    }

    public WorldBuilderCustomLevel smoothWorldTest(int times){
        return smoothWorld(times);
    }

    public WorldBuilderCustomLevel makeLvlWithPattern(String pattern){
        return makeFloor(pattern);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

    public Tile[][][] getTiles() {
        return tiles;
    }
}