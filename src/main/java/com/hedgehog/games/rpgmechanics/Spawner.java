package com.hedgehog.games.rpgmechanics;

import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.creatures.CreatureMaker;
import com.hedgehog.games.inventory.ItemMaker;
import com.hedgehog.games.world.World;

import java.util.List;

public class Spawner {

    private CreatureMaker creatureMaker;
    private ItemMaker itemMaker;
    private World world;
    private Creature finalBoss;
    private Creature player;
    private List<String> messages;
    private FieldOfView fieldOfView;
    private int noOfCreaturesToSpawnPerLvl;
    private int noOfItemsToSpawnPerLvl;

    public Spawner
            (CreatureMaker creatureMaker, ItemMaker itemMaker, World world,
             List<String> messages, FieldOfView fieldOfView) {
        this.creatureMaker = creatureMaker;
        this.itemMaker = itemMaker;
        this.world = world;
        this.finalBoss = creatureMaker.newGiantDireBat(world.getDepth() - 1);
        this.player = creatureMaker.newPlayer(messages, fieldOfView);
        this.messages = messages;
        this.fieldOfView = fieldOfView;
        this.noOfItemsToSpawnPerLvl = 3;
    }

    public Creature createPlayer() {
        return player;
    }

    public Creature createBoss() {
        return finalBoss;
    }

    public void spawnItems() {

        for (int i = 0; i < world.getDepth(); i++) {
            for (int j = 0; j < noOfItemsToSpawnPerLvl; j++) {
                itemMaker.newRandomWeapon(i);
                itemMaker.newRandomArmor(i);
            }
        }
    }

    public void spawnCreatures() {
        forceSpawnCreature();
        spawnRandomCreatures(creatureMaker, noOfCreaturesToSpawnPerLvl);
    }


    private void forceSpawnCreature() {
        for (int i = 0; i < world.getDepth(); i++) {
            creatureMaker.newCorruptor(i, fieldOfView);
            creatureMaker.newCorruptor(i, fieldOfView);
        }
    }

    private void spawnRandomCreatures(CreatureMaker creatureMaker, int noOfMonstersPerLvl) {
        for (int worldDepth = 0; worldDepth < world.getDepth(); worldDepth++) {
            for (int monster = 1; monster <= noOfMonstersPerLvl; monster++) {
                spawnRandomMonster(creatureMaker, worldDepth);
            }
        }
    }

    private void spawnRandomMonster(CreatureMaker creatureMaker, int worldDepth) {
        int min = worldDepth + 1;
        int rndMonster = (min) + (int) (Math.random() * (5));
        switch (rndMonster) {
            case 1:
            case 2:
                creatureMaker.newTinyFungus(worldDepth);
                break;
            case 3:
            case 4:
                creatureMaker.newSmallFungus(worldDepth);
                break;
            case 5:
            case 6:
                creatureMaker.newFungus(worldDepth);
                break;
            case 7:
            case 8:
                creatureMaker.newCaveBat(worldDepth);
                break;
            case 9:
            case 10:
                creatureMaker.newDireBat(worldDepth);
                break;
        }
    }

    public void setNoOfCreaturesToSpawnPerLvl(int noOfCreaturesToSpawnPerLvl) {
        this.noOfCreaturesToSpawnPerLvl = noOfCreaturesToSpawnPerLvl;
    }

    public int getNoOfItemsToSpawnPerLvl() {
        return noOfItemsToSpawnPerLvl;
    }

    public void setNoOfItemsToSpawnPerLvl(int noOfItemsToSpawnPerLvl) {
        this.noOfItemsToSpawnPerLvl = noOfItemsToSpawnPerLvl;
    }
}
