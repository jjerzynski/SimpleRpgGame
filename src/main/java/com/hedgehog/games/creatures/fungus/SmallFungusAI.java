package com.hedgehog.games.creatures.fungus;

import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.creatures.CreatureAI;
import com.hedgehog.games.creatures.CreatureMaker;

public class SmallFungusAI extends CreatureAI {
    private CreatureMaker factory;
    private int fungiSpreadCount = 0;
    private int maxNoOfSpreads;
    private double fungiSpreadChance = 0.005;
    private int minSpreadLength;
    private int maximumSpreadLength;


    public SmallFungusAI(Creature creature, CreatureMaker factory){
        super(creature);
        this.factory = factory;
        this.maximumSpreadLength = 2;
        this.minSpreadLength = -2;
        this.maxNoOfSpreads = 3;
    }

    public void onUpdate() {
        double rnd = Math.random();
        checkSurroundings();
        if(fungiSpreadCount < maxNoOfSpreads && rnd < fungiSpreadChance) {
            spread();
        }
    }

    private void spread() {
        int x;
        int y;
        int z = creature.z;

        do{
            x = creature.x + randomizeCoordinate(minSpreadLength, maximumSpreadLength);
            y = creature.y + randomizeCoordinate(minSpreadLength, maximumSpreadLength);
        } while (this.creature.x == x && this.creature.y == y);


        if(!creature.canEnter(x, y, z))
            return;

        spreadSuccessful(x, y, z);
    }

    private void spreadSuccessful(int x, int y, int z) {
        creature.doAction("spawn new Tiny Fungus");
        Creature tinyFungus = factory.newTinyFungus(z);
        tinyFungus.x = x;
        tinyFungus.y = y;
        tinyFungus.z = z;
        fungiSpreadCount++;
    }
}
