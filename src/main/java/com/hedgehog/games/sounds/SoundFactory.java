package com.hedgehog.games.sounds;

import com.hedgehog.games.creatures.Creature;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.File;
//this is test for SoundFactory, and JavaFX sound classes.
//TODO something better and more exception resistant
public class SoundFactory {

    private void soundPack(Creature creature,
                           String attackSound, String deathSound, String walkSound,
                           String stairsSound, String digSound, String specialSound) {

        creature.attackSound = getSound(attackSound);
        creature.deathSound = getSound(deathSound);
        creature.walkSound = getSound(walkSound);
        creature.stairsSound = getSound(stairsSound);
        creature.digSound = getSound(digSound);
        creature.specialSound = getSound(specialSound);
    }

    @NotNull
    @Contract(pure = true)
    private String soundPath() {
        return "resources" + File.separator + "sfx" + File.separator;
    }

    private String getSound(String attackSound) {
        return String.join("", soundPath(), attackSound);
    }

    public void playerSounds(Creature creature) {
        soundPack(creature,
                "hit_sword.wav",
                "slain_hero.wav",
                null,
                "stairs_walk.wav",
                "dig_pickaxe.wav",
                null);
    }

    public void fungusSounds(Creature creature) {
        soundPack(creature,
                "mute.wav",
                "mute.wav",
                null,
                null,
                null,
                "special_fungus_spread.wav");
    }

    public void batSounds(Creature creature) {
        soundPack(creature,
                "hit_bat.wav",
                "mute.wav",
                null,
                null,
                null,
                "mute.wav"
        );
    }
}
