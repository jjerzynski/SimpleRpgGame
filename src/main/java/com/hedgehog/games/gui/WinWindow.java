package com.hedgehog.games.gui;

import asciiTable.AsciiTable;
import colors.Grey;
import colors.Yellow;
import com.hedgehog.games.interfaces.ApplicationWindow;

import java.awt.event.KeyEvent;

public class WinWindow implements ApplicationWindow {
    @Override
    public void displayOutput(AsciiTable console) {
        console.writeCenter("Congratulations, You won!", 14, Yellow.yellow, Grey.mistGray);
        console.writeCenter("-- PRESS ENTER TO RESTART --", 16);
    }

    @Override
    public ApplicationWindow respondToUserInput(KeyEvent key) {
        return key.getKeyCode() == KeyEvent.VK_ENTER ? new GameMenuWindow() : this;
    }
}
