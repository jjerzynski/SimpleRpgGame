package com.hedgehog.games.interfaces;

import java.awt.Color;

public interface WorldObject {

    char getSymbol();
    Color getColor();
    String getName();
    String toString();

}
