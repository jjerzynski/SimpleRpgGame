package com.hedgehog.games.creatures.fungus;

import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.creatures.CreatureAI;

public class TinyFungusAI extends CreatureAI {

    public TinyFungusAI(Creature creature){
        super(creature);
    }

    public void onUpdate() {
    }
}
