package com.hedgehog.games.creatures;

import com.hedgehog.games.inventory.Item;
import org.junit.Assert;
import org.junit.Test;
import com.hedgehog.games.world.World;
import com.hedgehog.games.world.WorldBuilderCustomLevel;

public class CreatureTest {

    @Test
    public void HpTest_10000times() {

        int noOfHpDice = 1;
        int hpDice = 10;
        boolean expectedResult = true;

        boolean result = true;

        for (int i = 0; i < 1000; i++) {

            Creature creature = new Creature.CreatureBuilder(null, null, 'X', noOfHpDice, hpDice).build();

            if (creature.getHitPoints() < 1) {
                result = false;
                break;
            }
            if (creature.getHitPoints() > 10) {
                result = false;
                break;
            }
        }
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void Attack_RndValue_10000times() {

        int noOfAttDice = 1;
        int attDice = 10;
        boolean expectedResult = true;
        boolean result = true;

        for (int i = 0; i < 1000; i++) {

            Creature attacker = new Creature.CreatureBuilder(null, "attacker", 'A', 1, 1)
                    .withNoOfAttDice(noOfAttDice).withAttDice(attDice).build();
            Creature defender = new Creature.CreatureBuilder(null, "defender", 'D', 1, 1)
                    .withHpBonus(10).build();
            attacker.attack(defender);

            int otherHp = defender.getHitPoints();

            if (otherHp < 1)
                result = false;
            if (otherHp > 10)
                result = false;

            Assert.assertEquals(expectedResult, result);

        }
    }

    @Test
    public void Attack_attValueEqualsArmor_1000times() {


        Creature attacker = new Creature.CreatureBuilder(null, "Attacker", 'A', 1, 1)
                .withNoOfAttDice(1).withAttDice(1).build();
        Creature defender = new Creature.CreatureBuilder(null, "Defender", 'D', 1, 1)
                .withArmorClass(1).build();

        int expectedResult = 1;

        for (int i = 0; i < 10000; i++) {
            attacker.attack(defender);
        }

        int result = defender.getHitPoints();
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void Attack_attValue_lessThan_Armor_10000times() {

        Creature attacker = new Creature.CreatureBuilder(null, "Attacker", 'A', 1, 1)
                .withNoOfAttDice(1).withAttDice(1).build();
        Creature defender = new Creature.CreatureBuilder(null, "Defender", 'D', 1, 1)
                .withArmorClass(2).build();

        int expectedResult = 1;

        for (int i = 0; i < 10000; i++) {
            attacker.attack(defender);
        }

        int result = defender.getHitPoints();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void Attack_attValue_greaterThan_Armor_10000times() {

        int expectedResult = 1;

        for (int i = 0; i < 10000; i++) {

            Creature attacker = new Creature.CreatureBuilder(null, "Attacker", 'A', 1, 1)
                    .withNoOfAttDice(1).withAttDice(1).withAttBonus(2).build();
            Creature defender = new Creature.CreatureBuilder(null, "Defender", 'D', 2, 1)
                    .withArmorClass(2).build();
            attacker.attack(defender);

            int result = defender.getHitPoints();

            Assert.assertEquals(expectedResult, result);
        }
    }

    @Test
    public void KillOther_test() {

        boolean expectedResult = true;
        boolean result = false;
        Creature attacker = new Creature.CreatureBuilder(null, "Attacker", 'A', 1, 1)
                .withNoOfAttDice(1).withAttDice(10).withAttBonus(1).build();
        Creature defender = new Creature.CreatureBuilder(null, "Defender", 'D', 1, 1)
                .build();
        attacker.attack(defender);

        int defenderHitPoints = defender.getHitPoints();

        if (defenderHitPoints < 1) result = true;

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void KillOther_test_10000times() {

        boolean expectedResult = true;

        for (int i = 0; i < 10000; i++) {
            boolean result = false;

            Creature attacker = new Creature.CreatureBuilder(null, "Attacker", 'A', 1, 1)
                    .withNoOfAttDice(1).withAttDice(10).withAttBonus(1).build();
            Creature defender = new Creature.CreatureBuilder(null, "Defender", 'D', 1, 1)
                    .build();
            attacker.attack(defender);

            int defenderHitPoints = defender.getHitPoints();

            if (defenderHitPoints < 1) result = true;

            Assert.assertEquals(expectedResult, result);

        }
    }

    @Test
    public void move_w() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 'T', 1, 1)
                .build();
        new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;

        moveCreature(creature, "w");

        boolean result = (creature.x == oldX - 1 && creature.y == oldY) ? true : false;

        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void move_e() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 'T', 1, 1)
                .build();
        new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;

        moveCreature(creature, "e");

        boolean result = (creature.x == oldX + 1 && creature.y == oldY) ? true : false;

        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void move_n() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 'T', 1, 1)
                .build();
        new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;

        moveCreature(creature, "n");

        boolean result = (creature.x == oldX && creature.y == oldY - 1) ? true : false;

        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void move_s() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 'T', 1, 1)
                .build();
        new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;

        moveCreature(creature, "s");

        boolean result = (creature.x == oldX && creature.y == oldY + 1) ? true : false;

        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void move_nw() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 'T', 1, 1)
                .build();
        new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;

        moveCreature(creature, "nw");

        boolean result = (creature.x == oldX - 1 && creature.y == oldY - 1) ? true : false;

        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void move_ne() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 'T', 1, 1)
                .build();
        new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;

        moveCreature(creature, "ne");

        boolean result = (creature.x == oldX + 1 && creature.y == oldY - 1) ? true : false;

        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void move_sw() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 'T', 1, 1)
                .build();
        new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;

        moveCreature(creature, "sw");

        boolean result = (creature.x == oldX - 1 && creature.y == oldY + 1) ? true : false;

        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void move_se() throws Exception {
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("floor").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 'T', 1, 1)
                .build();
        new CreatureAI(creature);
        world.addAt(creature, 1, 1, 0);

        int oldX = creature.x;
        int oldY = creature.y;
        boolean expectedResult = true;

        moveCreature(creature, "se");

        boolean result = (creature.x == oldX + 1 && creature.y == oldY + 1) ? true : false;

        Assert.assertEquals(expectedResult, result);

    }

    @Test
    public void dig_test(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1,1)
                .build();

        creature.dig(1,1,0);
        Assert.assertTrue(world.tile(1,1,0).isFloor());

    }

    @Test
    public void corrupt_can_test(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1,1)
                .build();

        Assert.assertTrue(creature.canCorrupt(0,0,0));
        Assert.assertTrue(creature.canCorrupt(1,1,0));
    }

    @Test
    public void corrupt_cant_test(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 1,1)
                .build();

        Assert.assertTrue(creature.canCorrupt(1,1,0));
        creature.corrupt(1,1,0);
        Assert.assertFalse(creature.canCorrupt(1,1,0));

    }

    @Test
    public void corrupted_test_dmgEqualsReduceMaxHp(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 4,1)
                .build();

        creature.corrupted(3,3);

        Assert.assertTrue(creature.getHitPoints() == 1);
        Assert.assertTrue(creature.getMaxHitPoints() == 1);

    }

    @Test
    public void corrupted_test_dmgLessReduceMaxHp(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 4,1)
                .build();

        creature.corrupted(2,3);

        Assert.assertTrue(creature.getHitPoints() == 1);
        Assert.assertTrue(creature.getMaxHitPoints() == 1);

    }

    @Test
    public void corrupted_test_dmgAboveReduceMaxHp(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 4,1)
                .build();

        creature.corrupted(3,2);

        Assert.assertTrue(creature.getHitPoints() == 1);
        Assert.assertTrue(creature.getMaxHitPoints() == 2);

    }

    @Test
    public void corrupted_test_reduceMaxHPEqualsMaxHP(){
        World world = new WorldBuilderCustomLevel(1).makeLvlWithPattern("onewall").build();
        Creature creature = new Creature.CreatureBuilder(world, "test", 't', 4,1)
                .build();

        world.addAtEmptyLocation(creature,0);

        creature.corrupted(3,4);

        Assert.assertTrue(world.getCreaturesList().size() == 0);

    }

    @Test
    public void creature_specialMoves_1000times(){
        for (int i = 0; i < 1000; i++) {
            corrupt_can_test();
            corrupt_cant_test();
            corrupted_test_dmgAboveReduceMaxHp();
            corrupted_test_dmgEqualsReduceMaxHp();
            corrupted_test_dmgLessReduceMaxHp();
            corrupted_test_reduceMaxHPEqualsMaxHP();
            dig_test();
        }
    }

    @Test
    public void moveTests_1000times() throws Exception {
        for (int i = 0; i < 1000; i++) {
            move_s();
            move_n();
            move_e();
            move_w();
            move_nw();
            move_ne();
            move_se();
            move_sw();
        }
    }

    @Test
    public void creatureStatistics_str() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withAttributes("").build();

        int expectedResult = 10;
        int result = creature.attributes().getStrength();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creatureStatistics_agi() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withAttributes("").build();

        int expectedResult = 10;
        int result = creature.attributes().getAgility();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creatureStatistics_sta() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withAttributes("").build();

        int expectedResult = 10;
        int result = creature.attributes().getStamina();
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creatureStatistics_int() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withAttributes("").build();

        int expectedResult = 10;
        int result = creature.attributes().getIntellect();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creatureStatistics_spi() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withAttributes("").build();

        int expectedResult = 10;
        int result = creature.attributes().getSpirit();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_changeStrength() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withAttributes("").build();

        int expectedResult = 20;
        int result = creature.attributes().changeStrength(10);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_changeAgility() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withAttributes("").build();

        int expectedResult = 20;
        int result = creature.attributes().changeAgility(10);


        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_changeStamina() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withAttributes("").build();

        int expectedResult = 20;
        int result = creature.attributes().changeStamina(10);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_changeIntellect() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withAttributes("").build();

        int expectedResult = 20;
        int result = creature.attributes().changeIntellect(10);
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_changeSpirit() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withAttributes("").build();

        int expectedResult = 20;
        int result = creature.attributes().changeSpirit(10);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_statisticsTest_1000times(){
        for (int i = 0; i < 1000; i++) {
            creature_changeAgility();
            creature_changeIntellect();
            creature_changeStamina();
            creature_changeSpirit();
            creature_changeStrength();
            creatureStatistics_agi();
            creatureStatistics_int();
            creatureStatistics_spi();
            creatureStatistics_sta();
            creatureStatistics_str();
        }
    }

    @Test
    public void creature_inventory_size1() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withInventory(1).build();

        Assert.assertTrue(creature.inventory.getItems().length == 1);
    }

    @Test
    public void creature_inventory_size0() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withInventory(0).build();

        Assert.assertTrue(creature.inventory.getItems().length == 0);
    }

    @Test
    public void creature_inventory_sizeBelow0() {
        Creature creature = new Creature.CreatureBuilder(null, "test", 't', 1, 1)
                .withInventory(-1).build();

        Assert.assertTrue(creature.inventory.getItems().length == 0);
    }

    @Test
    public void creature_inventory_pickupItem() {
        World world = new WorldBuilderCustomLevel(3, 3, 1).makeLvlWithPattern("floor").build();
        Creature hero = new Creature.CreatureBuilder(world, "test", 't', 1, 1)
                .withInventory(1).build();
        Item item = new Item.RandomItemBuilder("armor").build();

        world.addAt(item,1,1, 0);
        world.addAt(hero,1,1,0);
        hero.pickupItem();


        Assert.assertTrue(item.equals(hero.inventory.get(0)));
        Assert.assertTrue(hero.inventory.get(0) != null);
        Assert.assertTrue(world.item(1,1,0) == null);

    }

    @Test
    public void creature_inventory_destroyItem() {
        World world = new WorldBuilderCustomLevel(3, 3, 1).makeLvlWithPattern("floor").build();
        Creature hero = new Creature.CreatureBuilder(world, "test", 't', 1, 1)
                .withInventory(1).build();
        Item item = new Item.RandomItemBuilder("armor").build();

        world.addAt(item,1,1, 0);
        world.addAt(hero,1,1,0);
        hero.pickupItem();
        Assert.assertTrue(hero.inventory.get(0) != null);

        hero.removeItemFromEq(item);

        Assert.assertTrue(hero.inventory.get(0) == null);
        Assert.assertTrue(world.item(1,1,0) == null);

    }

    @Test
    public void creature_inventoryTest_1000times(){
        for (int i = 0; i < 1000; i++) {
            creature_inventory_size0();
            creature_inventory_size1();
            creature_inventory_sizeBelow0();
            creature_inventory_destroyItem();
            creature_inventory_pickupItem();
        }
    }


    //helper
    private void moveCreature(Creature creature, String text) {
        switch (text) {
            case "w":
                creature.moveBy(-1, 0, 0);
                break;  //W
            case "e":
                creature.moveBy(1, 0, 0);
                break;   //E
            case "n":
                creature.moveBy(0, -1, 0);
                break;   //N
            case "s":
                creature.moveBy(0, 1, 0);
                break;   //S
            case "nw":
                creature.moveBy(-1, -1, 0);
                break;  //NW
            case "ne":
                creature.moveBy(1, -1, 0);
                break;   //NE
            case "sw":
                creature.moveBy(-1, 1, 0);
                break;  //SW
            case "se":
                creature.moveBy(1, 1, 0);
                break;   //SE
        }
    }
}