package com.hedgehog.games.gui;

import asciiTable.AsciiTable;
import colors.Green;
import colors.Red;
import com.hedgehog.games.interfaces.ApplicationWindow;

import java.awt.event.KeyEvent;


public class LoseWindow implements ApplicationWindow {

    LoseWindow(){

    }

    @Override
    public void displayOutput(AsciiTable console) {
        console.writeCenter("             ..ooo@@@XXX%%%xx..            ",1);
        console.writeCenter("          .oo@@XXX%x%xxx..     ` .         ",2);
        console.writeCenter("      o@X%..                  ..ooooooo    ",3);
        console.writeCenter("    .@X%x.                 ..o@@^^   ^^@@o ",4);
        console.writeCenter("  .ooo@@@@@@ooo..      ..o@@^          @X% ",5);
        console.writeCenter("  o@@^^^     ^^^@@@ooo.oo@@^             % ",6);
        console.writeCenter(" xzI    -*--      ^^^o^^        --*-     % ",7);
        console.writeCenter(" @@@o     ooooooo^@@^o^@X^@oooooo     .X%x ",8);
        console.writeCenter("I@@@@@@@@@XX%%xx  ( o@o )X%x@ROMBASED@@@X%x",9);
        console.writeCenter("I@@@@XX%%xx  oo@@@@X% @@X%x   ^^^@@@@@@@X%x",10);
        console.writeCenter(" @X%xx     o@@@@@@@X% @@XX%%x  )    ^^@X%x ",11);
        console.writeCenter("  ^   xx o@@@@@@@@Xx  ^ @XX%%x    xxx      ",12);
        console.writeCenter("        o@@^^^ooo I^^ I^o ooo   .  x       ",13);
        console.writeCenter("        oo @^ IX      I   ^X  @^ oo        ",14);
        console.writeCenter("        IX     U  .        V     IX        ",15);
        console.writeCenter("         V     .           .     V         ",16);
        console.writeCenter("Sometimes you die... ", 18, Red.crimson);
        console.writeCenter("-- PRESS ENTER TO RESTART --", 20, Green.darkLime);
    }

    @Override
    public ApplicationWindow respondToUserInput(KeyEvent key) {
        return key.getKeyCode() == KeyEvent.VK_ENTER ? new GameMenuWindow() : this;
    }

}
