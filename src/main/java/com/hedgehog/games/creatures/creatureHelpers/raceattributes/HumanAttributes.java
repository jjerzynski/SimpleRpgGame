package com.hedgehog.games.creatures.creatureHelpers.raceattributes;

public class HumanAttributes extends CreatureAttributes{
    private int strength = 10;
    private int agility = 10;
    private int stamina = 10;
    private int intellect = 10;
    private int spirit = 10;

    public int getStrength() {
        return strength;
    }

    public int getAgility() {
        return agility;
    }

    public int getStamina() {
        return stamina;
    }

    public int getIntellect() {
        return intellect;
    }

    public int getSpirit() {
        return spirit;
    }

}
