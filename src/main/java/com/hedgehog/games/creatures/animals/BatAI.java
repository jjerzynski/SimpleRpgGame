package com.hedgehog.games.creatures.animals;

import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.creatures.CreatureAI;

public class BatAI extends CreatureAI {

    private int flyRangeMin;
    private int flyRangeMax;

    public BatAI(Creature creature) {
        super(creature);
        this.flyRangeMin = -1;
        this.flyRangeMax = 1;
    }

    public void onUpdate(){
        wander(flyRangeMin, flyRangeMax, 2);

    }
}
