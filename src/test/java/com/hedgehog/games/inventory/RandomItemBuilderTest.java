package com.hedgehog.games.inventory;

import com.hedgehog.games.inventory.Item;
import org.junit.Assert;
import org.junit.Test;

public class RandomItemBuilderTest {

    @Test
    public void createRandomWeapon_test(){

        boolean expectedResult = false;
        boolean result = true;

        Item randomItem = new Item.RandomItemBuilder("weapon").build();
        if(randomItem != null){
            result = false;
        }

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void createRandomArmor_test(){
        boolean expectedResult = false;
        boolean result = true;

        Item randomItem = new Item.RandomItemBuilder("armor").build();
        if(randomItem != null){
            result = false;
        }
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void createRandomNullItem_test(){
        char expectedResult = (char)189;
        Item randomItem = new Item.RandomItemBuilder(null).build();

        char result = randomItem.getSymbol();
        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void createRandomItem_wrongCategory_test(){
        char expectedResult = (char)155;
        Item randomItem = new Item.RandomItemBuilder("something").build();

        char result = randomItem.getSymbol();
        Assert.assertEquals(expectedResult,result);
    }

    @Test
    public void randomItemBuilder_10000times(){
        for (int i = 0; i < 1000; i++) {
            createRandomWeapon_test();
            createRandomArmor_test();
            createRandomItem_wrongCategory_test();
            createRandomNullItem_test();
        }
    }
}