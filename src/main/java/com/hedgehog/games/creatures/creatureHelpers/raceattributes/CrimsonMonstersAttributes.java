package com.hedgehog.games.creatures.creatureHelpers.raceattributes;

public class CrimsonMonstersAttributes extends CreatureAttributes{
    private int strength = 15;
    private int agility = 10;
    private int stamina = 20;
    private int intellect = 12;
    private int spirit = 20;

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getAgility() {
        return agility;
    }

    @Override
    public int getStamina() {
        return stamina;
    }

    @Override
    public int getIntellect() {
        return intellect;
    }

    @Override
    public int getSpirit() {
        return spirit;
    }

}
