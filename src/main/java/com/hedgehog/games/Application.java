package com.hedgehog.games;

import javax.swing.*;

import asciiTable.AsciiTable;
import com.hedgehog.games.interfaces.AppVersion;
import com.hedgehog.games.interfaces.ApplicationWindow;
import javafx.embed.swing.JFXPanel;
import com.hedgehog.games.gui.GameMenuWindow;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Application extends JFrame implements KeyListener {

    private AsciiTable console;
    private ApplicationWindow applicationWindow;

    Application(){
        super();
        AppVersion version = new AppVersion();
        this.setTitle(version.getVersion());
        new JFXPanel();
        console = new AsciiTable(100,30);
        add(console);

        setMinimumSize(new Dimension(905,515));
        setPreferredSize(new Dimension(905,515));
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        pack();

        applicationWindow = new GameMenuWindow();
        addKeyListener(this);
        validate();
        repaint();
    }

    public void repaint(){
        console.clear();
        applicationWindow.displayOutput(console);
        super.validate();
        super.repaint();
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        applicationWindow = applicationWindow.respondToUserInput(e);
        repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }
}
