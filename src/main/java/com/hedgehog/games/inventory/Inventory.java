package com.hedgehog.games.inventory;

public class Inventory {

    private Item[] items;

    public Inventory(int inventorySize) {
            this.items = new Item[inventorySize];
    }

    public void add(Item item){
        for (int i = 0; i < items.length; i++) {
            if(items[i] == null){
                items[i] = item;
                break;
            }
        }
    }

    public void remove(Item item){
        for (int i = 0; i < items.length; i++) {
            if(items[i] == item){
                items[i] = null;
                return;
            }
        }
    }

    public boolean isFull(){
        int size = 0;
        for (Item item : items) {
            if (item != null) {
                size++;
            }
        }
        return size == items.length;
    }

    public Item[] getItems() {
        return items;
    }

    public Item get(int i){
        return items[i];
    }
}
