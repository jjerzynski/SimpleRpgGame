package com.hedgehog.games.gui;

import colors.Green;
import colors.Red;
import colors.Yellow;
import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.creatures.CreatureMaker;
import com.hedgehog.games.rpgmechanics.FieldOfView;
import com.hedgehog.games.interfaces.ApplicationWindow;
import com.hedgehog.games.inventory.ItemMaker;
import com.hedgehog.games.rpgmechanics.Spawner;
import com.hedgehog.games.world.World;
import com.hedgehog.games.world.WorldBuilder;
import asciiTable.AsciiTable;
import org.jetbrains.annotations.Nullable;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

//class will responsible for showing the dungeon and all it's inhabitants, it will also
//response for player input
public class GameWindow implements ApplicationWindow {

    private ApplicationWindow subWindow;
    private World world;
    private Creature finalBoss;
    private Creature player;
    private int screenWidth;
    private int screenHeight;
    private List<String> messages;
    private FieldOfView fieldOfView;
    private Spawner spawner;

    public GameWindow(){
        screenWidth = 100; //80
        screenHeight = 25;  //21
        initGame();

    }

    private void initGame() {
        createWorld();
        this.fieldOfView = new FieldOfView(world);
        this.messages = new ArrayList<>();
        CreatureMaker creatureMaker = new CreatureMaker(world);
        ItemMaker itemMaker = new ItemMaker(world);
        this.spawner = new Spawner(creatureMaker, itemMaker, world, messages, fieldOfView);
        spawner.setNoOfCreaturesToSpawnPerLvl(16);
        spawnWorldObjects();
    }

    private void spawnWorldObjects() {
        spawner.spawnCreatures();
        spawner.spawnItems();
        this.player = spawner.createPlayer();
        this.finalBoss = spawner.createBoss();
    }

    private void createWorld(){
        world = new WorldBuilder(120, 35, 5).makeCaves(3).build();
    }

    private void displayTiles(AsciiTable console, int width, int height){

        fieldOfView.update(player.x, player.y, player.z, player.getLineOfSight());

        for(int x = 0; x < screenWidth; x++){
            for(int y = 0; y < screenHeight; y++){
                int posX = x + width;
                int posY = y + height;

                if (player.canSee(posX, posY, player.z))
                    console.write(world.symbol(posX, posY, player.z), x, y, world.color(posX, posY, player.z));
                else
                    console.write(fieldOfView.tile(posX, posY, player.z).getSymbol(), x, y, Color.darkGray);
            }
        }
    }

    @Override
    public void displayOutput(AsciiTable console) {
        int left = getScrollX();
        int top = getScrollY();

        displayStatusBarInfos(console);
        displayTiles(console, left, top);
        displayMessages(console, messages);
        console.write(player.getSymbol(), player.x - left, player.y - top);

        if(subWindow != null){
            subWindow.displayOutput(console);
        }

    }

    private void displayStatusBarInfos(AsciiTable console) {
        String location = String.format("loc: %3d,%3d,%3d", player.x, player.y, player.z);
        String showHP = String.format("Hit Points %3d/%3d", player.getHitPoints(), player.getMaxHitPoints());
        String showAttackValue = String.format("Attack:  %3s", player.getAttValue());
        String showDefenseValue = String.format("Armor: %3d", player.getArmorClass());
        String showCaveDepth = String.format("Cave depth: %3d", player.z + 1);

        for (int i = 0; i < screenWidth ; i++) {
            for (int j = 25; j < 30; j++) {
                console.write(' ', i, j, Color.darkGray, Color.DARK_GRAY);
            }
        }

        writeStatusBar(console, location, showHP, showAttackValue, showDefenseValue, showCaveDepth);

    }

    private void writeStatusBar(AsciiTable console, String location, String showHP, String showAttackValue,
                                String showDefenseValue, String showCaveDepth) {
        console.write(showCaveDepth, 1, 25, Yellow.darkGold, Color.darkGray);
        console.write(location, 1,26, Yellow.darkGold, Color.darkGray);
        console.write(showAttackValue,1,27, Yellow.yellow, Color.darkGray);
        console.write(showDefenseValue, 1, 28, Green.darkLime, Color.darkGray);
        console.write(showHP, 1,29, Red.crimson, Color.darkGray);
    }

    private void displayMessages(AsciiTable console, List<String> messages) {
        int top = screenHeight +5 - messages.size();

        for(int i = 0; i < messages.size(); i++){
            console.writeCenter(messages.get(i), top + i);
        }
        messages.clear();
    }

    @Override
    public ApplicationWindow respondToUserInput(KeyEvent e) {
        if (subWindow != null) {
            subWindow = subWindow.respondToUserInput(e);
        } else {
            switch (e.getKeyCode()) {           //by directions of the World
                case KeyEvent.VK_NUMPAD4:
                    player.moveBy(-1, 0, 0);
                    break;  //W
                case KeyEvent.VK_NUMPAD6:
                    player.moveBy(1, 0, 0);
                    break;   //E
                case KeyEvent.VK_NUMPAD8:
                    player.moveBy(0, -1, 0);
                    break;   //N
                case KeyEvent.VK_NUMPAD2:
                    player.moveBy(0, 1, 0);
                    break;   //S
                case KeyEvent.VK_NUMPAD7:
                    player.moveBy(-1, -1, 0);
                    break;  //NW
                case KeyEvent.VK_NUMPAD9:
                    player.moveBy(1, -1, 0);
                    break;   //NE
                case KeyEvent.VK_NUMPAD1:
                    player.moveBy(-1, 1, 0);
                    break;  //SW
                case KeyEvent.VK_NUMPAD3:
                    player.moveBy(1, 1, 0);
                    break;   //SE
                case KeyEvent.VK_NUMPAD5:
                    player.update();
                    if (player.getHitPoints() < player.getMaxHitPoints()) player.modifyHp(1);
                    break;   //HoldPosition
                case KeyEvent.VK_B:
                    subWindow = new BackpackWindow(player, this);
                    break;
                case KeyEvent.VK_ESCAPE:
                    return new GameMenuWindow();
            }

            switch (e.getKeyChar()) {
                case ',':
                    player.moveBy(0, 0, -1);
                    break;
                case '.':
                    player.moveBy(0, 0, 1);
                    break;
                case 'p':
                    player.pickupItem();
                    break;
            }
        }
        world.update();

        ApplicationWindow x = checkWinCondition();
        if (x != null) return x;

        return this;
    }

    @Nullable
    private ApplicationWindow checkWinCondition() {
        if (player.getHitPoints() < 1) {
            return new LoseWindow();
        }
        if(finalBoss.getHitPoints() < 1){
            return new WinWindow();
        }
        return null;
    }

    private int getScrollX(){
        return Math.max(0, Math.min(player.x - screenWidth / 2, world.getWidth() - screenWidth));
    }

    private int getScrollY() {
        return Math.max(0, Math.min(player.y - screenHeight / 2, world.getHeight() - screenHeight));
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getScreenHeight() {
        return screenHeight;
    }
}
