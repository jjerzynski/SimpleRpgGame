package com.hedgehog.games.creatures.creatureHelpers;

import com.hedgehog.games.creatures.creatureHelpers.raceattributes.*;
import org.junit.Assert;
import org.junit.Test;

public class AttributesTest {

    @Test
    public void defaultAttributesTest_str(){
        Attributes attributes = new Attributes("");
        CreatureAttributes expected = new CreatureAttributes();

        int expectedResult = expected.getStrength();
        int result = attributes.getStrength();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void defaultAttributesTest_agi(){
        Attributes attributes = new Attributes("");
        CreatureAttributes expected = new CreatureAttributes();

        int expectedResult = expected.getAgility();
        int result = attributes.getAgility();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void defaultAttributesTest_sta(){
        Attributes attributes = new Attributes("");
        CreatureAttributes expected = new CreatureAttributes();

        int expectedResult = expected.getStamina();
        int result = attributes.getStamina();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void defaultAttributesTest_int(){
        Attributes attributes = new Attributes("");
        CreatureAttributes expected = new CreatureAttributes();

        int expectedResult = expected.getIntellect();
        int result = attributes.getIntellect();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void defaultAttributesTest_spi(){
        Attributes attributes = new Attributes("");
        CreatureAttributes expected = new CreatureAttributes();

        int expectedResult = expected.getSpirit();
        int result = attributes.getSpirit();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void nullAttributesTest_str(){
        Attributes attributes = new Attributes(null);
        CreatureAttributes expected = new CreatureAttributes();

        int expectedResult = expected.getStrength();
        int result = attributes.getStrength();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void nullAttributesTest_agi(){
        Attributes attributes = new Attributes(null);
        CreatureAttributes expected = new CreatureAttributes();

        int expectedResult = expected.getAgility();
        int result = attributes.getAgility();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void nullAttributesTest_sta(){
        Attributes attributes = new Attributes(null);
        CreatureAttributes expected = new CreatureAttributes();

        int expectedResult = expected.getStamina();
        int result = attributes.getStamina();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void nullAttributesTest_int(){
        Attributes attributes = new Attributes(null);
        CreatureAttributes expected = new CreatureAttributes();

        int expectedResult = expected.getIntellect();
        int result = attributes.getIntellect();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void nullAttributesTest_spi(){
        Attributes attributes = new Attributes(null);
        CreatureAttributes expected = new CreatureAttributes();

        int expectedResult = expected.getSpirit();
        int result = attributes.getSpirit();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void human_agi(){
        Attributes attributes = new Attributes("human");
        CreatureAttributes expected = new HumanAttributes();

        int expectedResult = expected.getAgility();
        int result = attributes.getAgility();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void fungus_sta(){
        Attributes attributes = new Attributes("fungus");
        CreatureAttributes expected = new FungusAttributes();

        int expectedResult = expected.getStamina();
        int result = attributes.getStamina();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void animal_int(){
        Attributes attributes = new Attributes("animal");
        CreatureAttributes expected = new AnimalAttributes();

        int expectedResult = expected.getIntellect();
        int result = attributes.getIntellect();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void crimson_spi(){
        Attributes attributes = new Attributes("crimson");
        CreatureAttributes expected = new CrimsonMonstersAttributes();

        int expectedResult = expected.getSpirit();
        int result = attributes.getSpirit();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_abilities_change_str(){
        Attributes attributes = new Attributes(null);


        int expectedResult = 20;
        int result = attributes.changeStrength(10);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_abilities_change_agi(){
        Attributes attributes = new Attributes(null);

        int expectedResult = 20;
        int result = attributes.changeAgility(10);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_abilities_change_sta(){
        Attributes attributes = new Attributes(null);

        int expectedResult = 20;
        int result = attributes.changeStamina(10);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_abilities_change_int(){
        Attributes attributes = new Attributes(null);

        int expectedResult = 20;
        int result = attributes.changeIntellect(10);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_abilities_change_spi(){
        Attributes attributes = new Attributes(null);

        int expectedResult = 20;
        int result = attributes.changeSpirit(10);

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_setAbilities_str(){
        Attributes attributes = new Attributes(null);

        int expectedResult = 20;
        attributes.setStrength(20);
        int result = attributes.getStrength();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_setAbilities_agi(){
        Attributes attributes = new Attributes(null);

        int expectedResult = 20;
        attributes.setAgility(20);
        int result = attributes.getAgility();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_setAbilities_sta(){
        Attributes attributes = new Attributes(null);

        int expectedResult = 20;
        attributes.setStamina(20);
        int result = attributes.getStamina();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_setAbilities_int(){
        Attributes attributes = new Attributes(null);

        int expectedResult = 20;
        attributes.setIntellect(20);
        int result = attributes.getIntellect();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void creature_setAbilities_spi(){
        Attributes attributes = new Attributes(null);

        int expectedResult = 20;
        attributes.setSpirit(20);
        int result = attributes.getSpirit();

        Assert.assertEquals(expectedResult, result);
    }
}