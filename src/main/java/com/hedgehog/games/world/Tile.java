package com.hedgehog.games.world;

import java.awt.Color;
import org.jetbrains.annotations.Contract;

public enum Tile {

    FLOOR((char)250, new Color(160,82,45)),
    WALL((char)219, new Color(160,82,45)),
    TUNNEL((char)176, new Color(100,50,50)),
    CORRUPTED_WALL((char)219, new Color(128, 0, 128)),
    CORRUPTED_GROUND('#', new Color(128, 0, 128)),
    UNKNOWN(' ', Color.black),
    BOUNDS('x', Color.black),
    STAIRSUP('<', Color.white),
    STAIRSDOWN('>', Color.white),
    EXIT((char)157, Color.yellow);

    private char symbol;
    private Color color;

    Tile(char symbol, Color color){
        this.symbol = symbol;
        this.color = color;


    }

    @Contract(pure = true)
    public boolean isDiggable(){
        return this == WALL;
    }

    @Contract(pure = true)
    public boolean isFloor(){
        return this != WALL && this != BOUNDS && this != CORRUPTED_WALL;
    }

    @Contract(pure = true)
    public boolean isCorruptedGround(){ return this == CORRUPTED_GROUND;}

    @Contract(pure = true)
    public boolean isCorruptedWall(){return this == CORRUPTED_WALL;}

    @Contract(pure = true)
    public char getSymbol() {
        return symbol;
    }

    @Contract(pure = true)
    public Color getColor() {
        return color;
    }



}
