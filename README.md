## Simple RPG Game

Simple RPG Game to aplikacja, która została napisana podczas nauki Javy 8 SE w [Craftin' Code](https://www.craftincode.com/). 
Aplikacja ta ma przypominać gry RPG z lat 80. ubiegłego wieku, takie jak Dwarf Fortress czy Hero Quest. 
Zamiast typowego interfejsu graficznego, wszystko wyświetlane jest w sposób imitujący konsolę.

**UWAGA:**
- Na branchu Master znajduje się najnowsza w pełni działająca wersja aplikacji;
- Na branchu Dev znajduje się aplikacja, która ma nowe usprawnienia (w teorii działające);
- Na branchu Features znajduje się najnowszy kod, wraz z nowymi usprawnieniami, 
które mogą zawierać błędy lub są w trakcie pisania.

**Spis treści:**
1. Wykorzystane biblioteki i narzędzia - [idź](https://gitlab.com/jjerzynski/SimpleRpgGame/blob/master/README.md#1-wykorzystane-biblioteki-i-narz%C4%99dzia)
2. Opis funkcjonalności - [idź](https://gitlab.com/jjerzynski/SimpleRpgGame/blob/master/README.md#2-opis-funkcjonalno%C5%9Bci)
3. Znane błędy i niedociągnięcia - [idź](https://gitlab.com/jjerzynski/SimpleRpgGame/blob/master/README.md#3-znane-b%C5%82%C4%99dy-i-inne-niedoci%C4%85gni%C4%99cia)
4. Czego można się spodziewać w przyszłych wersjach - [idź](https://gitlab.com/jjerzynski/SimpleRpgGame/blob/master/README.md#4-czego-mo%C5%BCna-si%C4%99-spodziewa%C4%87-w-przysz%C5%82ych-wersjach)
5. Gdybym miał napisać grę od nowa to co bym zmienił + wnioski - [idź](https://gitlab.com/jjerzynski/SimpleRpgGame/blob/master/README.md#5-gdybym-mia%C5%82-napisa%C4%87-gr%C4%99-od-nowa-to-co-bym-zmieni%C5%82)
6. Wybrane featury i ich kod - [idź](https://gitlab.com/jjerzynski/SimpleRpgGame/blob/master/README.md#6-wybrane-featurey-i-ich-kod)
7. Screeny z aplikacji - [idź](https://gitlab.com/jjerzynski/SimpleRpgGame/blob/master/README.md#7-screenshoty-z-rozgrywki)
8. Podziękowania dla [Craftin' Code](http://www.craftincode.com/) we Wrocławiu - [idź](https://gitlab.com/jjerzynski/SimpleRpgGame/blob/master/README.md#8-podzi%C4%99kowania-dla-craftin-code)
9. Moje opublikowane projekty - [idź](https://gitlab.com/jjerzynski/SimpleRpgGame/blob/master/README.md#9-moje-opublikowane-projekty)

English version soon, but not too soon


#### 1. Wykorzystane biblioteki i narzędzia
- Maven
- JUnit (obecne pokrycie testami ~60% klas, 45% linijek kodu).
- JetBrains Annotations
- Elementry Swinga i JavaFX (tylko to co niezbędne)
- RLHelper - Własnoręcznie napisana, prosta biblioteka dla Swinga, dodająca nowe kolory.
- Ascii-Table: Prosta biblioteka, która pozwala stworzyć tablicę dla znaków 
Ascii i wyświetlać je oraz nakładać na siebie.
- Wzorce projektowe: Strategy, Builder, Iterator oraz standardowo OOP 
(interfejsy, dziedziczenie, abstrakcja itd.). Dodatkowo starałem się stosować do 
zasad SOLID, YAGNI i KISS. Jednak nie zawsze i wszędzie udało mi się to osiągnąć.

Dodatkowo w grze wykorzystałem następujące algorytmy:
- Callular Automata dla generowania podziemi;
- Bresenham' Line Algorithm dla tworzenia pola widzenia;


Oczywiście w implementacji powyższych dwóch algorytmów pomógł mi Wujek Google 
oraz strona [www.java-gaming.org](http://www.java-gaming.org/)

Pokrycie kodu testami: (obecnie wynosi ~73% klas i ~63% linii kodu)

[![Coverage](https://i.imgur.com/Fn6Av94.png)](https://i.imgur.com/Fn6Av94.png)

#### 2. Opis funkcjonalności
W obecnym stanie gra jest ciągle rozwijana. Mogę powiedzieć śmiało, że to co 
trudne już za mną, a jeszcze cięższe przede mną (mi. Skrypt do śledzenia i 
atakowania gracza). Jednak w przybliżeniu, około 40% gry jest skończone.

Obecnie gracz może: 
- Poruszać się - klawiaturą numeryczną;
- Kopać tunele - trzeba 'wejść' w ścianę;
- Walczyć - trzeba wejść na przeciwnika;
- Podnosić przedmioty do plecaka i usuwać przedmioty z plecaka (uwaga: przedmioty 
są niszczone, a nie upuszczane. Jest to zamierzone działanie, ponieważ chcę 
wymusić na graczu ocenę przydatności znalezionych rzeczy)

Dodatkowo:
- Przedmioty (nazwy, rodzaje, rzadkość) są generowane losowo.
- Poziomy są generowane proceduralnie. W tym celu został zaimplementowany 
algorytm o nazwie "Cellular Automata". Szczegółowy opis znajdziesz 
[tutaj](http://www.roguebasin.com/index.php?title=Cellular_Automata_Method_for_Generating_Random_Cave-Like_Levels).
- Wszystkie potwory na wszystkich poziomach są symulowane jednocześnie. Co to oznacza? 
Ponieważ jest to gra turowa, to po graczu, swoją akcję ma każdy potwór. 
W standardowych grach z lat 80tych, swoją turę dostawali przeciwnicy, którzy 
znajdują się na tym samym poziomie co bohater. W mojej wersji, wszystkie potwory 
dostają swoją turę. Jest to celowe działanie, gdyż gracz ma mieć wybór - czy 
zwiedza dokładnie, zdobywa przedmioty itp. czy ryzykuje i rusza w głąb podziemi 
walczyć z potworami zanim się namnożą.
- Dźwięki i odgłosy. Ponieważ ten feature został zaimplementowany na szybko z 
użyciem mało mi znanego JavaFX.


#### 3. Znane błędy i niedociągnięcia
Oczywiście, na obecnym etapie aplikacja posiada kilka błędów i rzeczy do poprawy.
- Klasa CreatureMaker jest napisania od nowa.
Ciężko przy nich powiedzieć, że zastosowałem się tam do Dobrych Praktyk Programowania.
- Lag przy pierwszym odtworzeniu dźwięku.
Pierwsze odtworzenie danego dźwięku powoduje mały 'lag', ale aplikacja przyjmuje 
dalsze komendy. Dlatego może zdarzyć się, że próbując powtórzyć ruch zostaniemy 
telepotowani gdzieś dalej.
- Przedmioty w ekwipunku zawsze wyświetlają się po lewej stronie ekranu, 
podczas gry tytuł ramki i opis zależnie od położenia gracza. Prawdopodobnie 
gdy to czytasz, ten błąd został usunięty.
- Testy - z pewnością można lepiej, więcej i dokładniej. Jednak sporo rzeczy było 
testowanych w tzw. międzyczasie, dlatego nie mam potrzeby pisania dodatkowego kodu.

#### 4. Czego można się spodziewać w przyszłych wersjach
Czyli tzw "TO DO LIST", albo "Milestony":
- Inne opcje generowanie podziemi;
- Agresywne potwory (poruszające się w stronę gracza i atakujące go);
- Eteryczne potwory (przechodzące przez ściany nie niszcząc jej)
- System głodu i pragnienia;
- System doświadczenia i rozwoju postaci (atuty, zdolności, czary);
- Różne klasy postaci (wojownik, mag, łucznik);
- Działające przedmioty;
- Miejsce (sloty) na ubranie przedmiotów (głowa, tors, broń, buty, spodnie itp);
- CEL! Inny niż zabicie super nietoperza w podziemiach;
- Przeklęte i niezidentyfikowane przedmioty;
- Achivementy (nagrody, specjalne znajdźki, wyzwania);
- Nowy, lepszy system znajdywania przedmiotów;
- Rodzaje ataków i potworów (np. Trucizny, atak zamrażający);
- Całkiem nowy system tworzenia postaci (statystyki np. Siła, Zręczność, Wiedza...);
- Całkiem nowy system walki (oparty na losowości i kostkach jak w papierowych RPG);
- Docelowo ma być 20 do 30 poziomów ze specjalnym przeciwnikiem co 5-10 poziomów;

#### 5. Gdybym miał napisać grę od nowa, to co bym zmienił
- **1 - Interface graficzny:** z całą pewnością znalazłbym lepszą lub bardziej 
przyjazną bibliotekę niż Ascii-Table do tworzenia tego typu gier.

**Wniosek:** Szybciej i łatwiej byłoby zrobić grę na pełnym interfejsie 
graficznym, a nie symulowac konsolę. 

- **2 - Plan i projekt:** zaprojektowanie tak dużej aplikacji to dla mnie nie 
lada wyzwanie. Nie ukrywam, że jestem zadowolony z uzyskanego efektu. Wiem też,
że mogłoby być lepiej. Gdy piszę kolejne linijki kodu to przychodzą mi do głowy 
kolejne pomysły na dodatkowe rzeczy, które zazwyczaj wypadałoby napisać w 
istniejącej już klasie, a te rozrasrtają się o kolejne metody. Niestety to 
zmniejsza czytelność kodu.

**Wniosek:** Bardziej bym wyspecjalizował klasy, lepiej rozpisał projekt i listę 
zadań, więcej czasu poświęcił na planowanie.

- **3 - System symulowania podziemii:** W obecnej chwili działa to w następujący 
sposób - Gracz robi ruch, a następnie wszystkie pozostałe potwory wykonują swoje 
akcje (na wszystkich poziomach). Taki był pierwszy pomysł. Jednak z biegiem 
czasu dostrzegam potencjalne problemy związane z wydajnością, jeżeli apliacja
rozrośnie się za bardzo.

**Wniosek:** Gdybym pisał od początku, to poziomy przechowywałbym w liście tablic 
2-wymiarowych i symulował tylko jeden poziom w danym czasie.

- **4 - DRY i SOLID:** Co prawda starałem się stosować do tych i innych zasad na 
tyle, na ile umiem. Jednak zanim poznałem wzorce projektowe powstało już sporo 
kodu m.in. klasy tworzące potwory i przedmioty. Nie jestem z nich dumny, ale 
spełniają swoja rolę. Na szczęście przy posaniu klasy do odtwarzania dźwięków 
uniknąłem takiego problemu.

**Wniosek:** Z pewnością użyłbym Fabryk lub Builderów.

- **5 - Line of Sight i Field of View** (zwane Fog of War): Być może 
zaimplementowałbym lepszy skrypt dla pola widzenia. Obecny należy do prostych 
algorytmów rozwiązyującyh ten problem, ale co za tym idzie, jednym z najwolniejszych.
Nie przewiduje kłopotów związanych z tym, ale z pewnościa spróbowałbym czegoś nowego.

**Wniosek:** Taki sam jak w pkt. 2, lepiej rozpisać projekt i/lub zapoznać się 
z dostępnymi rozwiązaniami

- **6 - TESTY:** Poisałbym testy od początku. Oszczędziłbym sporo czasu i nerwów 
w wyszukiwaniu błędów. Tutaj jednak muszę usprawiedliwić się w tym, że testy 
poznałem dopiero niedawno

**Wniosek:** Testy są dobre, warto poświęcić im więcej uwagi i opanować je w 
lepszym stopniu.

#### 6. Wybrane klasy i elementy kodu
Poniżej znajduje się kod odpowiedzialny za losowanie kostkami.

````
package rpgmechanics;

import java.util.Random;

public class DiceRoller {

    private Random roller;

    public DiceRoller() {
        this.roller = new Random();
    }

    public int diceRoll(int numberOfRolls, int diceSize){
        int result = 0;
        result = roll(numberOfRolls, diceSize, 0, result);
        return result;
    }

    public int diceRoll(int numberOfRolls, int diceSize, int bonusPerRoll){
        int result = 0;
        result = roll(numberOfRolls, diceSize, bonusPerRoll, result);
        return result;
    }

    private int roll(int numberOfRolls, int diceSize, int bonusPerRoll, int result) {
        if(numberOfRolls == 0 || diceSize == 0){
            return 0;
        } else {
            for (int i = 1; i <= numberOfRolls; i++) {
                result += roller.nextInt(diceSize) + 1;
            }
            result += bonusPerRoll;
            if(result < 0){
                return 0;
            }
            return result;
        }
    }
}

````
Poniżej znajduje się screen z Buildera, który jest wewnętrzną klasą w klasie 
Creature (każdy potwór oraz gracz jest instancją tej klasy)
````
    public static class CreatureBuilder{
        World world;
        String name;
        char symbol;
        Color color;

        int noOfHpDice;
        int hpDice;
        int hpBonus;
        int maxHitPoints;
        int hitPoints;

        int noOfAttDice;
        int attDice;
        int attBonus;

        int armorClass;
        int lineOfSight;
        int inventorySize;
        int expValue;
        DiceRoller diceRoller;


        public CreatureBuilder(World world, String name, char symbol,
                               int noOfHpDice, int hpDice) {
            this.diceRoller = new DiceRoller();
            this.world = world;
            this.name = name;
            this.symbol = symbol;
            this.noOfHpDice = noOfHpDice;
            this.hpDice = hpDice;
            this.maxHitPoints = diceRoller.diceRoll(noOfHpDice, hpDice);
            this.hitPoints = maxHitPoints;
            this.lineOfSight = 9;
        }

        CreatureBuilder withNoOfAttDice(int noOfAttDice){
            this.noOfAttDice = noOfAttDice;
            return this;
        }

        CreatureBuilder withAttDice(int attDice){
            this.attDice = attDice;
            return this;
        }

        CreatureBuilder withArmorClass(int armorClass){
            this.armorClass = armorClass;
            return this;
        }

        CreatureBuilder withColor(Color color){
            this.color = color;
            return this;
        }

        CreatureBuilder withHpBonus(int hpBonus){
            this.hpBonus = hpBonus;
            return this;
        }

        CreatureBuilder withAttBonus(int attBonus){
            this.attBonus = attBonus;
            return this;
        }

        CreatureBuilder withInventory(int inventorySize){
            this.inventorySize = inventorySize;
            return this;
        }

        CreatureBuilder withExpValue(int expValue){
            this.expValue = expValue;
            return this;
        }

        public Creature build(){
            return new Creature(this.world,this.name, this.symbol,this.color,this.noOfHpDice, this.hpDice, this.hpBonus,
            this.noOfAttDice, this.attDice, this.attBonus, this.armorClass, this.inventorySize, this.expValue);
        }
    }
````
Poniżej znajduje się kod z fabryki statystyk. Klasy, która korzysta z fabryki.
````
public class CreatureStatisticsFactory {

    @NotNull
    public static CreatureAttributes create(String name) {
        if (name == null) {
            return new CreatureAttributes();
        } else {
            switch (name) {
                case "fungus":
                    return new FungusAttributes();
                case "animal":
                    return new AnimalAttributes();
                case "crimson":
                    return new CrimsonMonstersAttributes();
                case "human":
                    return new HumanAttributes();
                default:
                    return new CreatureAttributes();
            }
        }
    }
}
````
````
public class Attributes {

    private int strength;
    private int agility;
    private int stamina;
    private int intellect;
    private int spirit;


    public Attributes(String race) {
        CreatureAttributes creatureAttributes = CreatureStatisticsFactory.create(race);
        this.agility = creatureAttributes.getAgility();
        this.intellect = creatureAttributes.getIntellect();
        this.spirit = creatureAttributes.getSpirit();
        this.stamina = creatureAttributes.getStamina();
        this.strength = creatureAttributes.getStrength();

    }
	
	...
````
Poniżej znajduje się wycinek kodu z klasy WorldBuilder, odpowidzialnej za wygenerowanie świata. 
````
public class WorldBuilder {

    private int width;
    private int height;
    private int depth;
    private Tile[][][] tiles;
    private int[][][] regions;
    private int nextRegion;


    public WorldBuilder(int width, int height, int depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.tiles = new Tile[width][height][depth];
        this.nextRegion = 1;
    }

    public World build() {
        return new World(tiles);
    }
    
    private WorldBuilder randomizeTiles() {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    tiles[x][y][z] = Math.random() < 0.55 ? Tile.FLOOR : Tile.WALL;
                }
            }
        }
        return this;
    }
    
    private WorldBuilder smoothWorld(int times) {
        Tile[][][] smoothedTiles = new Tile[width][height][depth];
        if(times == 0){
            return this;
        }
        for (int i = 0; i < times; i++) {
            smoothLevel(smoothedTiles);
        }
        return this;
    }

    private void smoothLevel(Tile[][][] smoothedTiles) {
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                for (int z = 0; z < depth; z++) {
                    smoothTile(smoothedTiles, x, y, z);
                }
            }
        }
        tiles = smoothedTiles;
    }

    private void smoothTile(Tile[][][] smoothedTiles, int x, int y, int z) {
        int floors = 0;
        int walls = 0;

        for (int xx = -1; xx < 2; xx++) {
            for (int yy = -1; yy < 2; yy++) {
                if (x + xx < 0 || x + xx >= width || y + yy < 0 || y + yy >= height)
                    continue;
                if (tiles[x + xx][y + yy][z] == Tile.FLOOR) {
                    floors++;
                } else {
                    walls++;
                }
            }
        }
        smoothedTiles[x][y][z] = floors > walls ? Tile.FLOOR : Tile.WALL;
    }
    
    //dalej jest kod odpowiedzialny za tworzenie schodów i usuwanie za małych jaskiń
````
Poniżej znajduje się screen ze skryptu wytyczającego line of sight.

````
package com.hedgehog.games.creatures;

import Point;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LineOfSight implements Iterable<Point>{

    private List<Point> pointList;

    protected LineOfSight(int x1, int y1, int x2, int y2){

        pointList = new ArrayList<>();
        LineOfSightAlgorithm(x1, y1, x2, y2);
    }

    private void LineOfSightAlgorithm(int x1, int y1, int x2, int y2) {
        int dx = Math.abs(x2 - x1);
        int dy = Math.abs(y2 - y1);
        int sx = x1 < x2 ? 1 : -1;
        int sy = y1 < y2 ? 1 : -1;
        int error = dx - dy;

        drawLine(x1, y1, x2, y2, dx, dy, sx, sy, error);
    }

    private void drawLine(int x1, int y1, int x2, int y2, int dx, int dy, int sx, int sy, int error) {
        while(true){
            pointList.add(new Point(x1, y1,0));

            if(x1 == x2 && y1 == y2) break;

            int error2 = error * 2;

            if(error2 > -dx){
                error -= dy;
                x1 += sx;
            }
            if (error2 < dx) {
                error += dx;
                y1 += sy;
            }
        }
    }

    public Iterator<Point> iterator() {
        return pointList.iterator();
    }
}
````
Poniżej znajduje się kilka testów jednostkowych (z ponad 80 napisanych).
Wycinek testów dla Dice Rollera:
````
    @Test
    public void diceRoll_0d0() throws Exception {
        int noOfRolls = 0;
        int diceSize = 0;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_1d0() throws Exception {
        int noOfRolls = 1;
        int diceSize = 0;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_0d6() throws Exception {
        int noOfRolls = 0;
        int diceSize = 6;
        int expectedResult = 0;

        DiceRoller diceRoller = new DiceRoller();

        int result = diceRoller.diceRoll(noOfRolls, diceSize);

        assertEquals(expectedResult, result);
    }

    @Test
    public void diceRoll_1d6() throws Exception {
        int noOfRolls = 1;
        int diceSize = 6;
        boolean expectedResult = true;
        boolean result = false;

        DiceRoller diceRoller = new DiceRoller();

        int rollValue = diceRoller.diceRoll(noOfRolls, diceSize);
        if(rollValue >= 1 && rollValue <= 6) result = true;

        assertEquals(expectedResult, result);
    }
````

Poniżej wycinek z testów generycznego tworzenia świata, po tych testach znajdują 
się testy świata stworzonego na konkretnych warunkach, aby móc przetestować 
'wygładzanie' czyli zaimplementowany algorytm Callular Automata. W przyszłości 
dopisze proforma testy tworzenia przejść i pozostałych metod, jednak na chwilę 
obecną nie było to potrzebne, ponieważ testowałem to wiele razy manualnie.
````
@Test
    public void GenericWorld_notNull() {
        int w = 3;
        int h = 3;
        int tilesNotNull = 0;
        WorldBuilder wb = new WorldBuilder(w, h, 1).makeCaves(0);

        int expectedResult = 9;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (wb.getTile(i, j, 0) != null)
                    tilesNotNull++;
            }
        }

        int result = tilesNotNull;

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void GenericWorld_caveToSmall() {
        int w = 5;
        int h = 5;
        int floorTiles = 0;
        WorldBuilder wb = new WorldBuilder(w, h, 1).makeCaves(0);

        int expectedResult = 0;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (wb.getTile(i, j, 0) == Tile.FLOOR)
                    floorTiles++;
            }
        }

        int result = floorTiles;
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void GenericWorld_optimalCaveSize() {
        int w = 20;
        int h = 20;
        int floors = 0;
        WorldBuilder wb = new WorldBuilder(w, h, 1).makeCaves(0);

        boolean expectedResult = true;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (wb.getTile(i, j, 0) == Tile.FLOOR)
                    floors++;
            }
        }

        boolean result = floors >= 25;
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void GenericWorlds_notOnlyFloors(){
        int w = 20;
        int h = 20;
        int floors = 0;
        WorldBuilder wb = new WorldBuilder(w, h, 1).makeCaves(3);

        boolean expectedResult = true;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (wb.getTile(i, j, 0) != Tile.FLOOR)
                    floors++;
            }
        }

        boolean result = floors < w*h;
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void GenericWorlds_notOnlyWalls(){
        int w = 20;
        int h = 20;
        int walls = 0;
        WorldBuilder wb = new WorldBuilder(w, h, 1).makeCaves(3);

        boolean expectedResult = true;

        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (wb.getTile(i, j, 0) != Tile.WALL)
                    walls++;
            }
        }

        boolean result = walls < w*h;
        Assert.assertEquals(expectedResult, result);
    }
````
Poniżej wycinek z testów atakowania się potworów.

````
    @Test
    public void Attack_attValueEqualsArmor_1000times() {


        Creature attacker = new Creature.CreatureBuilder(null, "Attacker", 'A', 1, 1)
                .withNoOfAttDice(1).withAttDice(1).build();
        Creature defender = new Creature.CreatureBuilder(null, "Defender", 'D', 1, 1)
                .withArmorClass(1).build();

        int expectedResult = 1;

        for (int i = 0; i < 10000; i++) {
            attacker.attack(defender);
        }

        int result = defender.getHitPoints();
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void Attack_attValue_lessThan_Armor_10000times(){

        Creature attacker = new Creature.CreatureBuilder(null, "Attacker", 'A', 1, 1)
                .withNoOfAttDice(1).withAttDice(1).build();
        Creature defender = new Creature.CreatureBuilder(null, "Defender", 'D', 1, 1)
                .withArmorClass(2).build();

        int expectedResult = 1;

        for (int i = 0; i < 10000 ; i++) {
            attacker.attack(defender);
        }

        int result = defender.getHitPoints();

        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void Attack_attValue_greaterThan_Armor_10000times(){

        int expectedResult = 1;

        for (int i = 0; i < 10000 ; i++) {

            Creature attacker = new Creature.CreatureBuilder(null, "Attacker", 'A', 1, 1)
                    .withNoOfAttDice(1).withAttDice(1).withAttBonus(2).build();
            Creature defender = new Creature.CreatureBuilder(null, "Defender", 'D', 2, 1)
                    .withArmorClass(2).build();
            attacker.attack(defender);

            int result = defender.getHitPoints();

            Assert.assertEquals(expectedResult, result);
        }
    }
````
#### 7. Screenshoty z rozgrywki:

- Gra się rozpoczęła. Jak widać, znamy nasza pozycję, głębokość... 
ale nie znamy świata. No to ruszamy....
[![Start_game](https://i.imgur.com/HbGnt1S.png)](https://i.imgur.com/HbGnt1S.png)


- Poruszając się po świecie, wszystko co jest poza naszym polem widzenia spowija mgła. 
Nie zobaczymy pod nią żadnych potworów czy przedmiotów. Widać tylko elementy terenu 
(podłogi, ściany i coś fioletowego). Znalezione w międzyczasie przedmioty możemy 
schować do plecaka.
[![Move](https://i.imgur.com/Lcwb7u4.png)](https://i.imgur.com/Lcwb7u4.png)


- Jak widać, nasza Zbroja Płytowa, zmieściła sie w plecaku obok znalezionej 
wcześniej maczugi.
[![Backpack](https://i.imgur.com/S2KqJZc.png)](https://i.imgur.com/S2KqJZc.png)


- Oczywiście jednym ze sposobów unieknięcia walki jest kopanie tuneli. Możemy też 
w ten sposób dotrzeć do innych jaskiń.
[![Dig](https://i.imgur.com/vySxWl5.png)](https://i.imgur.com/vySxWl5.png)


- Jeżeli jednak będziemy chcieli walczyć, wystarczy znaleźć przeciwnika i 
zaatakować go. Widać tutaj, że nasz przeciwnik rozprzestrzenia korupcję, która 
jest łatwa do ominięcia, ale niebezpieczna. Dodatkowo znaleźliśmy kolejny 
przedmiot (zielony) oraz innego przeciwnika (czerwony).
[![Fight](https://i.imgur.com/bb4wRrc.png)](https://i.imgur.com/bb4wRrc.png)


- Po uporaniu się z przeciwnikami i podniesieniu przedmiotu, odnajdujemy zejście 
na dół. Możemy oczywiście zwiedzać dalej. Mając jednak na uwadze, że potwory 
mogą się rozprzestrzeniać, nawet jak nie ma nas w pobliżu, schodzę poziom niżej.
[![Escape](https://i.imgur.com/X5A84rz.png)](https://i.imgur.com/X5A84rz.png)


- I jesteśmy na poziomie drugim. Zabawa zaczyna się od nowa. Poziom jest nieznany, 
ale widzimy już pierwszego przeciwnika....
[![Przedmiot](https://i.imgur.com/OXuDNqf.png)](https://i.imgur.com/OXuDNqf.png)

 
#### 8. Podziękowania dla Craftin' Code
W tym miejscu chciałbym podziękować zespołowi 
[Craftin' Code](http://www.craftincode.com/#about-us) - Kubie i Adrianowi, 
u których miałem okazję uczyć się i szlifować swoje umiejętności programowania
w Javie 8 SE. Dzięki nim poznałem mi. GITa i Mavena, wzorce projektowe, 
nauczyłem się dobrych praktyk... lista jest długa i sięga poza zakres Javy i 
programowania. Dziękuję za wytrwałość, pomoc, naukę, bycie mentorem, zostawanie 
ze mną po zajęciach by nakierować mnie na rozwiązanie. 

**Także, jeszcze raz, dziękuję Adrian, dziękuję Jakub!**

#### 9. Moje opublikowane projekty

1. [Event Manager](https://gitlab.com/jjerzynski/Spring-MeetupService.git) - (w produkcji) Aplikaca do zarządzania wydarzeniami takimi jak konferencje, wykłady, konwenty;
2. [Simple RPG Game](https://gitlab.com/jjerzynski/SimpleRpgGame) - (w produkcji) Okienkowa gra RPG stylizowana na produkcje z lat '80-'90, mój największy projekt;
3. [Weather App](https://gitlab.com/jjerzynski/SimpleWeatherApp) - (projekt zakończony) Konsolowa aplikacja pogodowa oparna na REST;
4. [Mastermind](https://gitlab.com/jjerzynski/MasterMindConsole) - (projekt zakończony) Gra logiczna z lat '80;
5. [TicTacToe](https://gitlab.com/jjerzynski/TicTacToeGame) - (projekt zakończony) Kóło i krzyżyk - projekt na zaliczenie zajęc w Craftin' Code, mój pierwszy projekt;



## English version
Coming soon... but not too soon...


