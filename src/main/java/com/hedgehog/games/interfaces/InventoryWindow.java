package com.hedgehog.games.interfaces;

import asciiTable.AsciiTable;
import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.gui.GameWindow;
import com.hedgehog.games.inventory.Item;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public abstract class InventoryWindow implements ApplicationWindow {

    private GameWindow gameWindow;
    protected Creature player;
    private String backpackSlots;

    protected abstract String getVerb();
    protected abstract boolean isAcceptable(Item item);
    protected abstract ApplicationWindow use(Item item);

    public InventoryWindow(Creature player, GameWindow gameWindow) {
        this.player = player;
        this.backpackSlots = "1234567890";
        this.gameWindow = gameWindow;
    }

    public void displayOutput(AsciiTable console){

        ArrayList<String> inventorySlot = getListOfItems();
        int backpackCoordinateY = 20;
        int inventorySize = inventorySlot.size();
        int backpackTitleCoordinateY = backpackCoordinateY - inventorySize -1;
        int backpackCoordinateX = 25;
        int itemCoordinateY = backpackCoordinateY - inventorySize;
        int itemCoordinateX = backpackCoordinateX +1;

        prepareBackpack(console, inventorySlot, inventorySize, itemCoordinateX, itemCoordinateY);
        showBackpackFrames(console, backpackTitleCoordinateY, backpackCoordinateX, backpackCoordinateY);
        showBackpackItems(console, inventorySlot, itemCoordinateX, itemCoordinateY);
        console.repaint();
    }

    private void prepareBackpack(AsciiTable console, ArrayList<String> inventorySlot,
                                 int invSize, int itemCoordinateX, int itemCoordinateY) {
        if(inventorySlot.size() > 0){
            console.clear(' ', itemCoordinateX, itemCoordinateY, 45, invSize, Color.gray, Color.gray);
        }
    }

    private void showBackpackItems(AsciiTable console, ArrayList<String> inventorySlot,
                                   int itemCoordinateX, int itemCoordinateY) {
        for (String slot : inventorySlot) {
            console.write(' ', itemCoordinateX-1,itemCoordinateY, Color.darkGray, Color.gray);
            console.write(slot, itemCoordinateX, itemCoordinateY++,Color.white,Color.gray);
        }
    }

    private void showBackpackFrames(AsciiTable console, int backpackTitleCoordinateY,
                                    int backpackCoordinateX, int backpackCoordinateY) {
        console.clear(' ', backpackCoordinateX,backpackCoordinateY,45,1);
        console.write("          ========= BACKPACK =======          ",
                backpackCoordinateX,backpackTitleCoordinateY,
                        Color.green, Color.gray);

        console.write("   Pick item to remove, or esc to escape      ",
                backpackCoordinateX,backpackCoordinateY,
                        Color.white, Color.gray);
    }

    private ArrayList<String> getListOfItems() {
        ArrayList<String> inventorySlot = new ArrayList<>();
        Item[] inventory = player.getInventory().getItems();

        for (int i = 0; i < inventory.length ; i++) {
            Item item = inventory[i];

            if(item == null || !isAcceptable(item)){
                continue;
            }

            String backpackItem = backpackSlots.charAt(i) + ":  " + item.getSymbol() + "  --  " + item.getName();
            inventorySlot.add(backpackItem);
        }
        return inventorySlot;
    }

    public ApplicationWindow respondToUserInput(KeyEvent key) {
        char slot = key.getKeyChar();

        Item[] items = player.getInventory().getItems();

        if (backpackSlots.indexOf(slot) > -1
                && items.length > backpackSlots.indexOf(slot)
                && items[backpackSlots.indexOf(slot)] != null
                && isAcceptable(items[backpackSlots.indexOf(slot)])) {
            return use(items[backpackSlots.indexOf(slot)]);
        } else if (key.getKeyCode() == KeyEvent.VK_ESCAPE) {
            return null;
        } else {
            return this;
        }
    }
}
