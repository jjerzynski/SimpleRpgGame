package com.hedgehog.games.creatures;


import com.hedgehog.games.creatures.creatureHelpers.Attributes;
import com.hedgehog.games.interfaces.WorldObject;
import com.hedgehog.games.inventory.Inventory;
import com.hedgehog.games.inventory.Item;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import com.hedgehog.games.rpgmechanics.DiceRoller;
import com.hedgehog.games.world.Tile;
import com.hedgehog.games.world.World;

import java.awt.Color;

public class Creature implements WorldObject {

    private World world;
    private String name;
    private char symbol;
    private Color color;
    private CreatureAI ai;
    private Attributes attributes;

    public int x;
    public int y;
    public int z;

    public String attackSound;
    public String deathSound;
    public String walkSound;
    public String stairsSound;
    public String digSound;
    public String specialSound;

    private int noOfHpDice;
    private int hpDice;
    private int hpBonus;
    private int maxHitPoints;
    private int hitPoints;

    private int noOfAttDice;
    private int attDice;
    private int attBonus;

    private int armorClass;
    private int lineOfSight;

    private int inventorySize;
    private int experience;
    private int expValue;
    public Inventory inventory;
    private DiceRoller diceRoller;

    private Creature(World world, String name, char symbol, Color color,
                    int noOfHpDice, int hpDice, int hpBonus,
                    int noOfAttDice, int attDice, int attBonus, int armorClass,
                    int inventorySize, int expValue, Attributes attributes) {

        this.diceRoller = new DiceRoller();
        this.inventorySize = inventorySize;
        this.inventory = new Inventory(this.inventorySize);
        this.attributes = attributes;
        this.world = world;
        this.name = name;
        this.symbol = symbol;
        this.color = color;
        this.hpBonus = 0;
        this.maxHitPoints = diceRoller.diceRoll(noOfHpDice, hpDice, hpBonus);
        this.hitPoints = maxHitPoints;
        this.noOfAttDice = noOfAttDice;
        this.attDice = attDice;
        this.attBonus = attBonus;
        this.armorClass = armorClass;
        this.lineOfSight = 9;
        this.experience = 0;
        this.expValue = expValue;

    }

    public void pickupItem(){
        Item item = world.item(x, y, z);

        if(inventory.isFull() || item == null){
            doAction("grab at the ground");
        } else {
            doAction("pickup a %s", item.getName());
            inventory.add(item);
            world.remove(x, y, z);
        }
    }

    public void removeItemFromEq(Item item){
        doAction("destroy a " + item.getName());
        inventory.remove(item);
    }

    public boolean canSee(int worldX, int worldY, int worldZ) {
        return ai.canSee(worldX, worldY, worldZ);
    }

    public Tile tile(int worldX, int worldY, int worldZ) {
        return world.tile(worldX, worldY, worldZ);
    }

    private void notify(String message, Object... params) {
        try {
            ai.onNotify(String.format(message, params));
        }catch (NullPointerException exc){
        }
    }

    public boolean canEnter(int worldX, int worldY, int worldZ) {
        return world.tile(worldX, worldY, worldZ).isFloor() && world.creature(worldX, worldY, worldZ) == null;
    }

    public boolean canCorrupt(int worldX, int worldY, int worldZ) {
        return (!world.tile(worldX, worldY, worldZ).isCorruptedGround() &&
                !world.tile(worldX, worldY, worldZ).isCorruptedWall())
                && world.creature(worldX, worldY, worldZ) == null;
    }

    public void update() {
        ai.onUpdate();
    }

    public void dig(int worldX, int worldY, int worldZ) {
        makeSound(digSound);
        doAction("dig");
        world.dig(worldX, worldY, worldZ);
    }

    public void corrupt(int worldX, int worldY, int worldZ){
        doAction("corrupt");
        world.corrupt(worldX, worldY, worldZ);
    }

    public void moveBy(int creatureX, int creatureY, int creatureZ) {
        Tile tile = world.tile(x + creatureX, y + creatureY, z + creatureZ);

        if (creatureX == 0 && creatureY == 0 && creatureZ == 0) return;
        if (goDown(creatureZ, tile)) return;
        if (goUp(creatureZ, tile)) return;

        Creature other = world.creature(creatureX + x, creatureY + y, creatureZ + z);
        attackMove(creatureX, creatureY, creatureZ, other);
    }

    private void attackMove(int creatureX, int creatureY, int creatureZ, Creature other) {
        if (other == null) {
            ai.onEnter(creatureX + x, creatureY + y, creatureZ + z,
                    world.tile(x + creatureX, y + creatureY, z + creatureZ));
        } else {
            attack(other);
        }
    }

    private boolean goDown(int creatureZ, Tile tile) {
        if (creatureZ == -1) {
            if (tile == Tile.STAIRSDOWN) {
                doAction("walk up the stairs to level %d", z + creatureZ + 1);
                makeSound(stairsSound);

                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ignored) {}
            } else {
                doAction("try to go up but are stopped by cave ceiling");
                return true;
            }
        }
        return false;
    }

    private boolean goUp(int creatureZ, Tile tile) {
        if (creatureZ == 1) {
            if (tile == Tile.STAIRSUP) {
                doAction("walk down the stairs to level %d", z + creatureZ + 1);
                makeSound(stairsSound);
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException ignored) {}
            } else {
                doAction("try to go down but are stopped by cave floor");
                return true;
            }
        }
        return false;
    }

    void attack(Creature other) {

        makeSound(attackSound);
        int attackValue = diceRoller.diceRoll(noOfAttDice, attDice, attBonus);
        int damage = attackValue - other.getArmorClass();

        if(damage < 1) damage = 0;
        doAction("attack the '%s' for %d damage.", other.getName(), damage);
        other.modifyHp(-damage);
    }

    public void modifyHp(int amount) {

        hitPoints += amount;

        removeFromWorld();
    }

    private void removeFromWorld() {
        if (hitPoints < 1) {

            doAction("die");

            if(world != null)
            world.remove(this);
        }
    }

    public void doAction(String message, Object... params) throws NullPointerException {
        try {
            int radius = lineOfSight;
            for (int xx = -radius; xx < radius + 1; xx++) {
                for (int yy = -radius; yy < radius + 1; yy++) {
                    if (xx * xx + yy * yy > radius * radius)
                        continue;

                    Creature other = world.creature(x + xx, y + yy, z);

                    if (other == null)
                        continue;

                    if (other == this) {
                        other.notify("You " + message + ".", params);
                        continue;
                    }
                    if (other.getSymbol() == '@' && other.canSee(x, y, z)) {
                        makeSound(specialSound);
                        other.notify(String.format("The '%s' %s.", name, makeSecondPerson(message)), params);
                    }
                }
            }
        }catch (NullPointerException exc){
        }
    }

    public void corrupted(int dmg, int reduceMaxHp){
//        this.hitPoints = hitPoints - dmg;
        this.modifyHp(-dmg);
        this.maxHitPoints = maxHitPoints - reduceMaxHp;
        if(this.hitPoints > this.maxHitPoints){
            this.hitPoints = this.maxHitPoints;
        }
        removeFromWorld();
        doAction("are corrupted");
    }

    @NotNull
    private String makeSecondPerson(String text) {
        String[] words = text.split(" ");
        words[0] = words[0] + "s";

        StringBuilder builder = new StringBuilder();
        for (String word : words) {
            builder.append(" ");
            builder.append(word);
        }
        return builder.toString().trim();
    }

    private void makeSound(String soundName)  {
        try {
            Media sound = new Media(new File(soundName).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
        }catch (NullPointerException exc){
        }
    }

    public Creature creature(int worldX, int worldY, int worldZ) {
        return world.creature(worldX, worldY, worldZ);
    }

    //Getters and Setters
    void setCreatureAI(CreatureAI ai) {
        this.ai = ai;
    }

    public char getSymbol() {
        return symbol;
    }

    public Color getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public int getMaxHitPoints() {
        return maxHitPoints;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public String getAttValue() {
        return getNoOfAttDice() + "d" + getAttDice() + "+" + getAttBonus();

    }

    @Contract(pure = true)
    private int getNoOfAttDice() {
        return noOfAttDice;
    }

    @Contract(pure = true)
    private int getAttDice() {
        return attDice;
    }

    @Contract(pure = true)
    private int getAttBonus() {
        return attBonus;
    }

    public int getArmorClass() {
        return armorClass;
    }

    public int getLineOfSight() {
        return lineOfSight;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public Attributes attributes() {
        return attributes;
    }

    public static class CreatureBuilder{
        World world;
        String name;
        char symbol;
        Color color;

        int noOfHpDice;
        int hpDice;
        int hpBonus;
        int maxHitPoints;
        int hitPoints;

        int noOfAttDice;
        int attDice;
        int attBonus;

        int armorClass;
        int lineOfSight;
        int inventorySize;
        int expValue;
        Attributes attributes;
        DiceRoller diceRoller;

        public CreatureBuilder(World world, String name, char symbol,
                               int noOfHpDice, int hpDice) {
            this.diceRoller = new DiceRoller();
            this.world = world;
            this.name = name;
            this.symbol = symbol;
            this.noOfHpDice = noOfHpDice;
            this.hpDice = hpDice;
            this.maxHitPoints = diceRoller.diceRoll(noOfHpDice, hpDice);
            this.hitPoints = maxHitPoints;
            this.lineOfSight = 9;
        }

        public CreatureBuilder withNoOfAttDice(int noOfAttDice){
            this.noOfAttDice = noOfAttDice;
            return this;
        }

        public CreatureBuilder withAttDice(int attDice){
            this.attDice = attDice;
            return this;
        }

        public CreatureBuilder withArmorClass(int armorClass){
            this.armorClass = armorClass;
            return this;
        }

        public CreatureBuilder withColor(Color color){
            this.color = color;
            return this;
        }

        public CreatureBuilder withHpBonus(int hpBonus){
            this.hpBonus = hpBonus;
            return this;
        }

        public CreatureBuilder withAttBonus(int attBonus){
            this.attBonus = attBonus;
            return this;
        }

        public CreatureBuilder withInventory(int inventorySize){
            if(inventorySize > 0) {
                this.inventorySize = inventorySize;
            } else {
                this.inventorySize = 0;
            }
            return this;
        }

        public CreatureBuilder withAttributes(String attributes){
            this.attributes = new Attributes(attributes);
            return this;
        }

        public Creature build(){
            return new Creature(this.world,this.name, this.symbol,this.color,this.noOfHpDice, this.hpDice, this.hpBonus,
            this.noOfAttDice, this.attDice, this.attBonus, this.armorClass, this.inventorySize, this.expValue, this.attributes);
        }

    }

}
