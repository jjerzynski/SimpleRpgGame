package com.hedgehog.games.creatures.creatureHelpers;

import com.hedgehog.games.creatures.creatureHelpers.raceattributes.*;
import org.jetbrains.annotations.NotNull;

public class CreatureStatisticsFactory {

    @NotNull
    public static CreatureAttributes create(String name) {
        if (name == null) {
            return new CreatureAttributes();
        } else {
            switch (name) {
                case "fungus":
                    return new FungusAttributes();
                case "animal":
                    return new AnimalAttributes();
                case "crimson":
                    return new CrimsonMonstersAttributes();
                case "human":
                    return new HumanAttributes();
                default:
                    return new CreatureAttributes();
            }
        }
    }
}
