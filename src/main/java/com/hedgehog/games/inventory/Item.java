package com.hedgehog.games.inventory;

import com.hedgehog.games.inventory.ItemHelpers.ItemSymbol;
import com.hedgehog.games.interfaces.WorldObject;
import com.hedgehog.games.inventory.ItemHelpers.ItemSuffix;

import java.awt.*;

public class Item implements WorldObject {

    private char symbol;
    private Color color;
    private String name;
    private int attackValue;
    private int defenseValue;
    private int prefix;
    private int suffix;
    private String type;
    private String rarity;


    Item(char symbol, Color color, String name, int attackValue, int defenseValue) {
        this.symbol = symbol;
        this.color = color;
        this.name = name;
        this.attackValue = attackValue;
        this.defenseValue = defenseValue;
    }

    public void modifyAttackValue(int amount){
        attackValue += amount;
    }

    public void modifyDefenseValue(int amount){
        defenseValue += amount;
    }

    public int getAttackValue() {
        return attackValue;
    }

    public int getDefenseValue() {
        return defenseValue;
    }

    public char getSymbol() {
        return symbol;
    }

    public Color getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public static class ItemBuilder{
        char symbol;
        Color color;
        String name;
        int attackValue;
        int defenseValue;

        public ItemBuilder(char symbol, String name) {
            this.symbol = symbol;
            this.name = name;
        }

        public ItemBuilder withColor(Color color){
            this.color = color;
            return this;
        }

        public ItemBuilder withAttValue(int attackValue){
            this.attackValue = attackValue;
            return this;
        }

        public ItemBuilder withDefValue(int defenseValue){
            this.defenseValue = defenseValue;
            return this;
        }

        public Item build(){
            return new Item(this.symbol, this.color, this.name, this.attackValue, this.defenseValue);
        }
    }

    public static class RandomItemBuilder{
        char symbol;
        Color color;
        String coreName;
        String name;
        int attackValue;
        int defenseValue;
        int prefix;
        String suffix;
        String category;
        String rarity;
        ItemSymbol itemSymbol;
        ItemSuffix itemSuffix;

        public RandomItemBuilder(String itemCategory) {
            this.category = itemCategory;
            this.itemSymbol = new ItemSymbol();
            this.itemSuffix = new ItemSuffix();
            prepareElements();
        }

        private void prepareElements(){
            if(category == null){
                nullItem();
            } else if(!category.equals("weapon") && !category.equals("armor")) {
                oldPiecesOfAncientItem();
            } else {
                setCoreAndSymbol(category); //Two setters are merged together because symbol depends on coreName
                setRarityAndColor(); //Two setters are merged together because color depends on rarity
                setSuffix();
                createFullName();
            }
        }

        private void createFullName(){

            StringBuilder builder = new StringBuilder();
            this.name = builder.append(this.rarity).append(this.coreName).append(this.suffix).toString();
        }

        private void setSuffix(){
            double rnd = Math.random();

            if(rnd > 0.6) {
                this.suffix = itemSuffix.getSuffix();
            } else {
                this.suffix = "";
            }
        }

        private void setCoreAndSymbol(String category){
            if(category.equals("armor")) {
                setArmorCore();
            } else if(category.equals("weapon")){
                setWeaponCore();
            }
        }

        private void setWeaponCore(){
            double rarityRoll = Math.random();
            rndWeapon(rarityRoll);
        }

        private void rndWeapon(double rarityRoll) {
            if(rarityRoll < 0.166666){
                setMainStats("Staff", itemSymbol.W_STAFF(), 2, 0);
            } else if(rarityRoll < 0.333333){
                setMainStats("Polearm", itemSymbol.W_POLEARM(), 3,1);
            } else if(rarityRoll < 0.500000){
                setMainStats("Sword", itemSymbol.W_SWORD(), 4,0);
            } else if(rarityRoll < 0.666666){
                setMainStats("Axe", itemSymbol.W_AXE(),5,0);
            } else if(rarityRoll < 0.833333){
                setMainStats("Dagger", itemSymbol.W_DAGGER(), 1,0);
            } else if(rarityRoll <= 1.0){
                setMainStats("Mace", itemSymbol.W_MACE(), 3,0);
            }
        }

        private void setMainStats(String coreName, char itemSymbol, int attackValue, int defenseValue) {
            this.coreName = coreName;
            this.symbol = itemSymbol;
            this.attackValue = attackValue;
            this.defenseValue = defenseValue;
        }

        private void setArmorCore(){
            double rarityRoll = Math.random();
            rndArmorType(rarityRoll);
        }

        private void rndArmorType(double rarityRoll) {
            if(rarityRoll < 0.25){
                setMainStats("Cloth Armor", itemSymbol.A_CLOTH(), 0, 1);
            } else if(rarityRoll < 0.50){
                setMainStats("Leather Armor", itemSymbol.A_LEATHER(), 0, 2);
            } else if(rarityRoll < 0.75){
                setMainStats("Mail Armor", itemSymbol.A_MAIL(), 0, 3);
            } else if(rarityRoll <= 1.0) {
                setMainStats("Plate Armor", itemSymbol.A_PLATE(), 0, 4);
            }
        }

        private void setRarityAndColor() {
            double rarityRoll = Math.random();
            rndRarity(rarityRoll);
        }

        private void rndRarity(double rarityRoll){
            if(rarityRoll < 0.001){
                setRarity("Legendary ", Color.orange);
            } else if(rarityRoll < 0.01){
                setRarity("Epic ", Color.magenta);
            } else if(rarityRoll < 0.05){
                setRarity("Rare ", Color.blue);
            } else if(rarityRoll < 0.2){
                setRarity("Good ", Color.green);
            } else if(rarityRoll < 0.8){
                setRarity("", Color.white);
            } else {
                setRarity("Broken ", Color.gray);
            }
        }

        private void setRarity(String rarity, Color color) {
            this.rarity = rarity;
            this.color = color;
        }

        private void  nullItem(){
            setExceptionItem("Cursed Rod from Null City", itemSymbol.S_NULL(), Color.darkGray, 0,0);
        }

        private void  oldPiecesOfAncientItem(){
            setExceptionItem("old pieces of ancient device", itemSymbol.S_ANCIENT(), Color.darkGray, 0,0);
        }

        private void setExceptionItem(String name, char itemSymbol, Color color, int attackValue, int defenseValue) {
            this.name = name;
            this.symbol = itemSymbol;
            this.color = color;
            this.attackValue = attackValue;
            this.defenseValue = defenseValue;
        }

        public Item build(){
            return new Item(this.symbol, this.color, this.name, this.attackValue, this.defenseValue);
        }
    }
}
