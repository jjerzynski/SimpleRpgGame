package com.hedgehog.games.creatures.player;

import com.hedgehog.games.creatures.Creature;
import com.hedgehog.games.creatures.CreatureAI;
import com.hedgehog.games.rpgmechanics.FieldOfView;
import com.hedgehog.games.world.Tile;

import java.util.List;

public class PlayerAI extends CreatureAI {

    private List<String> messages;
    private FieldOfView fieldOfView;

    public PlayerAI(Creature creature, List<String> messages, FieldOfView fieldOfView){
        super(creature);
        this.messages = messages;
        this.fieldOfView = fieldOfView;

    }

    public void onEnter(int x, int y, int z, Tile tile) {
        if(tile.isCorruptedGround()){
            creature.corrupted(5,2);
            move(x, y, z);
        } else if (tile.isFloor()) {
            move(x, y, z);
        } else if (tile.isDiggable()) {
            creature.dig(x, y, z);
        }
    }

    private void move(int x, int y, int z) {
        creature.x = x;
        creature.y = y;
        creature.z = z;
    }

    public boolean canSee(int worldX, int worldY, int worldZ){
        return fieldOfView.isVisible(worldX, worldY, worldZ);
    }

    public void onNotify(String message){
        messages.add(message);
    }

    public int getX(){
        return creature.x;
    }

    public int getY(){
        return creature.y;
    }

    public int getZ(){
        return creature.z;
    }
}
